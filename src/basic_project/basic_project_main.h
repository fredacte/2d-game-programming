//	Author:			Cheung Kwok Wai
//	Description:	SDL Project Warpper
//	Date:			10/3/2012
#pragma once
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")
#pragma comment(lib, "SDL_ttf.lib")
#pragma comment(lib, "SDL_Image.lib")
#pragma comment(lib, "SDL_Mixer.lib")

#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )



#include <SDL.h>
#include <string>
#include <SDL_ttf.h>
#include <SDL_Image.h>
#include <SDL_Mixer.h>
#include <SDL_opengl.h>
#include "display_object.h"

#include <map>
#include <boost\format.hpp>

#define KEY_PRESSED(evt,SDLK) evt->key.keysym.sym == SDLK && evt->key.state == SDL_PRESSED
using namespace boost;


class Basic_SDL
{
private:
	bool			m_running;
	bool			m_swap_gl_buffer;
	bool			m_force_clean_up;
	SDL_Surface*	m_surf_display;


	int				m_delay;
	int				m_lastTimeElapsed;
	double			m_totalTimeElapsed;
	int				m_screen_width;
	int				m_screen_height;

	TTF_Font*		m_pFont;
	DisplayObjectPtr	m_currentScene;
	GL_Color		m_bkColor;

	typedef map<std::string,Texture> TexturePool;
	typedef map<std::string,TTF_Font*>	FontPool;

	static TexturePool  m_texturePool;
	static FontPool		m_fontPool;

	
	void GL_BeginDraw()
	{
		//depth test to z-ordering
		//
		glEnable(GL_DEPTH_TEST);
		glDepthFunc( GL_LESS );

		//turn blending on
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);     

		glAlphaFunc(GL_GREATER,0);
		
		glClearColor( m_bkColor.red, m_bkColor.green, m_bkColor.blue, m_bkColor.alpha );

		glViewport( 0, 0, m_screen_width, m_screen_height );

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();

		glOrtho(0.0f, m_screen_width, m_screen_height, 0.0f, -1.0f, 1.0f);

		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();
	}
protected:
	static Basic_SDL*		m_sdl_instance;

public:
	
	void GL_SetBackgroundColor(GL_Color color){	m_bkColor =color;}
	GL_Color GL_GetBackgroundColor(){return m_bkColor;}

	void SetForceCleanUp(){m_force_clean_up = true;}
	static TTF_Font* GL_AutoReleaseFont(const char* filename,int ftsize)
	{
		//if file has already load
		std::string key = str(format("%1%%2%")%filename%ftsize);
		FontPool::iterator it = m_fontPool.find(key);
		if(it != m_fontPool.end())
		{
			return it->second;
		}else{
			//load new one
			TTF_Font* ft = TTF_OpenFont(filename,ftsize);
			m_fontPool[key] = ft;
			return ft;
		}
	}
	
	//same logic with auto release surface
	static Texture GL_AutoReleaseTexture(std::string fileName){
		//if file has already load
		TexturePool::iterator it = m_texturePool.find(fileName);
		if(it != m_texturePool.end())
		{
			return it->second;
		}else{
			//load new one
			Texture texture = Texture::Load(fileName);
			m_texturePool[fileName] = texture;
			return texture;
		}
	}
	static Basic_SDL* GetInstance(){return m_sdl_instance;}
	int GetScreenWidth(){return m_screen_width;}
	int GetScreenHeight(){return m_screen_height;}
	int GetLastTimeElapsed(){return m_lastTimeElapsed;}
	double GetTotalTimeElapsed(){return m_totalTimeElapsed;}
	void Reset(){m_totalTimeElapsed = 0;m_lastTimeElapsed=0;}

	DisplayObjectPtr GetScene(){	return m_currentScene;}
	void SetScene(DisplayObjectPtr s){ m_currentScene = s;} 	

	SDL_Surface* GL_GetMainDisplay(){return m_surf_display;}
	void GL_ApplySurface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
	{
		//Make a temporary rectangle to hold the offsets
		SDL_Rect offset;

		//Give the offsets to the rectangle
		offset.x = x;
		offset.y = y;

		//Blit the surface

		if (source == NULL || destination == NULL )
			return;



		SDL_BlitSurface( source, NULL, destination, &offset );


	}
	inline Uint32 GL_CreateColor(GL_Colori c)
	{
		return SDL_MapRGBA(m_surf_display->format, c.r, c.g, c.b,c.a);

	}

	SDL_Surface* GL_CreateText(const char* str,GL_Colori c,TTF_Font* custom_font = NULL)
	{
		SDL_Color sdl_color;
		SDL_GetRGB(GL_CreateColor(c), m_surf_display->format, &sdl_color.r,&sdl_color.g, &sdl_color.b);
		return TTF_RenderText_Blended(custom_font == NULL ? m_pFont : custom_font,str,sdl_color);
	}
	void GL_PrintText(const char* str,int x,int y,GL_Colori color,bool alignCenterX = false,bool alignCenterY = false)
	{
		SDL_Surface* text = 0;
		text = GL_CreateText(str,color);
		GL_ApplySurface(alignCenterX ? x - text->w / 2 : x,alignCenterY ? y - text->h / 2 : y,text,m_surf_display);
		GL_FreeSurface(text);

	}
	inline int GL_DrawBackground(Uint32 dwColor)
	{
		SDL_Rect rect;
		rect.x = 0;
		rect.y = 0;
		rect.w = m_screen_width;
		rect.h = m_screen_height;

		SDL_FillRect(m_surf_display,&rect,dwColor);
		//SDL_UpdateRect(m_surf_display,0,0,m_screen_width,m_screen_height);
		return 1;

	}
	
	template <class T>
	T* GL_CreateSprite(SDL_Surface** surface_holder,const char* filename)
	{
		if(*surface_holder == NULL)	*surface_holder = GL_LoadImage(filename);

		return new T(*surface_holder);

	}

	inline void GL_DrawRootSprite()
	{
		m_currentScene->DrawChildren();
	}
	static void GL_FreeSurface(SDL_Surface* surface)
	{
		if(surface != NULL)
			SDL_FreeSurface(surface);
	}
	static void GL_FreeTexture(GLuint* tex)
	{
		if(*tex != 0)
			glDeleteTextures( 1, tex);
	}

	Mix_Chunk* GL_LoadSound(char *szFname)
	{
		Mix_Chunk *pChunk = NULL;

		pChunk = Mix_LoadWAV(szFname);

		if (pChunk == NULL)
		{
			fprintf(stderr, "Error loading sound %s: %s\n", szFname, Mix_GetError());
			exit(1);
		}

		return pChunk;
	}



	Basic_SDL(int w,int h,bool glMode)
	{
		m_screen_width = w;
		m_screen_height = h;
		m_running = true;
		m_surf_display = NULL;
		m_delay = 25;
		m_swap_gl_buffer = glMode;



		m_sdl_instance = this;
	}
	
	virtual bool OnInit()
	{

		if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			return false;
		}

		if((m_surf_display = SDL_SetVideoMode(m_screen_width, m_screen_height, 32, SDL_ANYFORMAT | SDL_HWSURFACE | SDL_OPENGL | SDL_DOUBLEBUF)) == NULL) {
			return false;
		}

		IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);

		m_currentScene = DisplayObjectPtr(new DisplayObject());
		m_currentScene->Update = true;

		TTF_Init();

		m_pFont = GL_AutoReleaseFont("Hobo.ttf",20);

		return true;
	}
	virtual void OnEvent(SDL_Event* evt)
	{
		m_currentScene->OnEvent(evt,m_lastTimeElapsed);
		if(evt->type == SDL_QUIT) {
			m_running = false;
		}
	}
	void Quit()
	{
		m_running = false;
	}


	virtual void OnLoop()
	{
		if(m_force_clean_up){
			m_currentScene->CleanUpChildRemoved();
			m_force_clean_up = false;
		}
		if(m_currentScene->Update)
			m_currentScene->OnUpdateChildren(m_lastTimeElapsed);
	}

	virtual void OnRender()
	{
		GL_DrawRootSprite();
	}

	virtual void OnCleanup()
	{
		m_currentScene.reset();

		//cleanup texture pool
		TexturePool::iterator it2 = m_texturePool.begin();
		while(it2 != m_texturePool.end())
		{
			glDeleteTextures( 1, &(it2->second.id) );
			it2 = m_texturePool.erase(it2);
		}

		//cleanup font pool
		FontPool::iterator it3 = m_fontPool.begin();
		while(it3 != m_fontPool.end())
		{
			TTF_CloseFont( it3->second );
			it3 = m_fontPool.erase(it3);
		}

		IMG_Quit();
		TTF_Quit();
		SDL_Quit();
	}

	int OnExecute()
	{
		if(OnInit() == false) {
			return -1;
		}

		SDL_Event Event;

		//when first run, elcapsed time is 0
		m_lastTimeElapsed = 0;
		m_totalTimeElapsed = 0;
		while(m_running) {

			int time = SDL_GetTicks();
			while(SDL_PollEvent(&Event)) {
				OnEvent(&Event);
			}

			OnLoop();

			if(m_swap_gl_buffer)
			{
				GL_BeginDraw();
			}
			else{
				GL_DrawBackground(GL_CreateColor(GL_Colori()));			
			}

			OnRender();

			if(m_swap_gl_buffer)
			{
				SDL_GL_SwapBuffers();
			}else{
				SDL_Flip( m_surf_display );
			}

			SDL_Delay(m_delay);

			//update time elapsed
			m_lastTimeElapsed = SDL_GetTicks() - time;

			if(m_currentScene->Update)
				m_totalTimeElapsed += m_lastTimeElapsed;
		}

		OnCleanup();

		return 0;
	}

};

//	Unused function
/*

	
	
	inline void GL_DrawBox(int x0, int y0, int x1, int y1, Uint32 dwColor,SDL_Surface* pDisplaySurface)
	{
		GL_DrawLine(x0,y0,x1,y0,dwColor,pDisplaySurface);
		GL_DrawLine(x0,y1,x1,y1,dwColor,pDisplaySurface);
		GL_DrawLine(x0,y0,x0,y1,dwColor,pDisplaySurface);
		GL_DrawLine(x1,y0,x1,y1,dwColor,pDisplaySurface);


	}
	void GL_DrawCircle(int x0, int y0, int radius, Uint32 dwColor,SDL_Surface* pDisplaySurface)
	{
		//Midpoint circle algorithm
		//Refer: http://en.wikipedia.org/wiki/Midpoint_circle_algorithm
		int f = 1 - radius;
		int ddF_x = 1;
		int ddF_y = -2 * radius;
		int x = 0;
		int y = radius;

		//error checking
		if (pDisplaySurface == NULL)	return;
		if (SDL_MUSTLOCK(pDisplaySurface) && SDL_LockSurface(pDisplaySurface) == -1)	return;


		GL_DrawPixel_NoLock(x0, y0 + radius,dwColor,pDisplaySurface);
		GL_DrawPixel_NoLock(x0, y0 - radius,dwColor,pDisplaySurface);
		GL_DrawPixel_NoLock(x0 + radius, y0,dwColor,pDisplaySurface);
		GL_DrawPixel_NoLock(x0 - radius, y0,dwColor,pDisplaySurface);

		while(x < y)
		{
			if(f >= 0)
			{
				y--;
				ddF_y += 2;
				f += ddF_y;
			}
			x++;
			ddF_x += 2;
			f += ddF_x;
			GL_DrawPixel_NoLock(x0 + x, y0 + y,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 - x, y0 + y,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 + x, y0 - y,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 - x, y0 - y,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 + y, y0 + x,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 - y, y0 + x,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 + y, y0 - x,dwColor,pDisplaySurface);
			GL_DrawPixel_NoLock(x0 - y, y0 - x,dwColor,pDisplaySurface);
		}

		// unlock the surface
		if (SDL_MUSTLOCK(pDisplaySurface)) SDL_UnlockSurface(pDisplaySurface);

	}

	void GL_DrawLine(int x0,int y0,int x1,int y1, Uint32 dwColor,SDL_Surface* pDisplaySurface)
	{

		char*	pData;

		//error checking
		if (pDisplaySurface == NULL)	return;
		if (SDL_MUSTLOCK(pDisplaySurface) && SDL_LockSurface(pDisplaySurface) == -1)	return;

		//clamp
		x0 = min(m_screen_width,max(0,x0));
		x1 = min(m_screen_width,max(0,x1));
		y0 = min(m_screen_height,max(0,y0));
		y1 = min(m_screen_height,max(0,y1));

		// jump to x0,y0
		pData = (char*)pDisplaySurface->pixels;
		int pixelOffset = pDisplaySurface->format->BytesPerPixel;
		int lineOffset =  pDisplaySurface->pitch;
		pData += (x0 * pixelOffset) + (y0 * lineOffset);

		//Bresenham's_line_algorithm
		//Refer:http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
		int dx = abs(x1-x0);
		int dy = abs(y1-y0);
		int sx = x0 < x1 ? 1 : -1;
		int sy = y0 < y1 ? 1 : -1;
		int err = dx-dy;

		for(;;)
		{
			memcpy(pData, &dwColor, pixelOffset);

			if(x0 == x1 && y0 == y1)
				break;
			int e2 = 2 * err;
			if(e2 > -dy){
				err = err - dy;
				x0 = x0 +sx;
				pData += sx * pixelOffset;
			}
			if(e2 < dx){
				err = err + dx;
				y0 = y0 + sy;
				pData += sy * lineOffset;
			}
		}

		if (SDL_MUSTLOCK(pDisplaySurface)) SDL_UnlockSurface(pDisplaySurface);
	}
	//Optimization - inner use without lock
	inline void GL_DrawPixel_NoLock(int iX, int iY, Uint32 dwColor, SDL_Surface* pDisplaySurface)
	{
		// copy from color to frame buffer
		memcpy( ((char*)pDisplaySurface->pixels + (iY * pDisplaySurface->pitch) +
			(iX * pDisplaySurface->format->BytesPerPixel)),
			&dwColor, pDisplaySurface->format->BytesPerPixel);
	}
	//for outer use with lock
	bool GL_DrawPixel(int iX, int iY, Uint32 dwColor, SDL_Surface* pDisplaySurface)
	{
		//char*	pData;			// temporary data pointer

		if (pDisplaySurface == NULL)	return false;

		// check if surface needs locking & Lock
		if (SDL_MUSTLOCK(pDisplaySurface) && SDL_LockSurface(pDisplaySurface) == -1)	return false;

		// draw pixel
		GL_DrawPixel_NoLock(iX,iY,dwColor,pDisplaySurface);

		// unlock the surface
		if (SDL_MUSTLOCK(pDisplaySurface)) SDL_UnlockSurface(pDisplaySurface);


		return true;
	}
*/