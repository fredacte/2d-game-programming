#pragma once
#include "..\sprite\sprite_text.h"
#include <typeinfo>
/// Helper Sprite to print text on screen
class Console : public Sprite_GL
{
private:
	static Console* m_instance;
	TTF_Font* m_font;
public:
	unsigned int Max_Stack;
	Console();

	static shared_ptr<Console> GetInstance();
	void Trace(std::string s);
};

