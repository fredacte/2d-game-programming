#include "..\sprite\sprite.h"

#include "..\basic_project\basic_project_main.h"
#include "..\basic_project\scene.h"

class Basic_project_with_scene : public Basic_SDL
{
protected:
	Scene* m_currentScene;
public:
	Basic_project_with_scene(int width,int height) : Basic_SDL(width,height){}

    virtual bool OnInit()
	{
		if(!Basic_SDL::OnInit()) return false;

		m_currentScene = NULL;
	}
	virtual void OnLoop()
	{
		Basic_SDL::OnLoop();
		if(m_currentScene != NULL)
		{
			bool isEndScene = m_currentScene->OnLoop();
			//get next scene
			if(isEndScene)
			{	
				//doing clean up
				m_currentScene = m_currentScene->OnSceneEnd();
				if(m_currentScene == NULL)
				{
					m_running = false;	//end game
				}
			}
		}
	}
	virtual void OnRender()
	{
		Basic_SDL::OnRender();
		if(m_currentScene != NULL)
		{
			m_currentScene->OnRender();
		}
	}
	virtual void OnEvent(SDL_Event* evt)
	{
		Basic_SDL::OnEvent(evt);
		if(m_currentScene != NULL)
		{
			m_currentScene->OnEvent(evt);
		}
	}
	virtual void OnCleanup()
	{
		if(m_currentScene != NULL)	delete m_currentScene;
		Basic_SDL::OnCleanup();
	}
	
	Scene* GetCurrentScene(){return m_currentScene;}
	void SetCurrentScene(Scene* scene){m_currentScene = scene;}
	
};
