#pragma once

#include <SDL.h>
#include <vector>
#include <boost\smart_ptr.hpp>
#include "..\basic_project\utility.h"

using namespace std;
using namespace boost;
class DisplayObject;

typedef shared_ptr<DisplayObject> DisplayObjectPtr;
typedef vector<DisplayObjectPtr> DisplayObjectCtr;
typedef DisplayObjectCtr::iterator DisplayObjectIt;

class EventListener;
typedef shared_ptr<EventListener> EventListenerPtr;
/// handle event of display object
class EventListener
{
public:

	virtual bool OnMouseMove	(SDL_Event* evt,DisplayObject* obj){return false;}
	virtual bool OnMouseDown	(SDL_Event* evt,DisplayObject* obj){return false;}
	virtual bool OnMouseUp		(SDL_Event* evt,DisplayObject* obj){return false;}
	virtual bool OnMouseOver	(SDL_Event* evt,DisplayObject* obj,bool isEndTouch){return false;}
	virtual ~EventListener(){}
};


/// basic drawing object, also work as container.
/// handle transformation, event handle.
class DisplayObject : public enable_shared_from_this<DisplayObject>
{
protected:

	DisplayObject*		m_father;
	DisplayObjectCtr	m_container;
	EventListenerPtr	m_listener;

	bool				m_mouse_inside;

public:
	// All these vairable is public because it is safe to modifiy.
	// Those variable were used frequently and direct access can improve performace.
	// all upper case 

	GL_Vec2		Position;
	GL_Vec2		Center;
	void*		User_Object;

	//default true
	bool Visible;
	bool Update;
	bool UpdateChild;
	bool HandleEvent;
	bool HandleEventChild;

	//
	// HandleEventMouse:			enable handle OnMouseUp,Down,Up event when mouse hit test success
	// HandleEventMouseNoHitTest:	handle mouse event without hit test.
	// HandleEventMouseMove:		enable OnMouseMove , this event will be triggered when 
	//
	// mouse cursor begin touch a  display object or end touch.
	// default values are false to reduce unnecessary calculation.
	bool HandleEventMouse;
	bool HandleEventMouseNoHitTest;
	bool HandleEventMouseMove;
	bool RemoveThis;

	// if true and multiple child display object were triggered in same time, 
	// mouse event will just response to the first call back function (MouseUp,MouseDown...ect)
	// which return true( means handle the event). often set true when work as ui button.
	// default is false
	bool HandleEventMouseLock;		

	void SetEventMouse(bool event_mouse,bool no_hit_test,bool mouse_move)
	{
		HandleEventMouse = event_mouse;
		HandleEventMouseNoHitTest = no_hit_test;
		HandleEventMouseMove = mouse_move;
	}
	inline EventListenerPtr SetEventListener(EventListenerPtr lst)
	{
		m_listener = lst;
		return m_listener;
	}
	inline EventListenerPtr SetEventListener(EventListener* lst)
	{
		return SetEventListener(EventListenerPtr(lst));
	}


	EventListenerPtr GetEventListener(){return m_listener;}
	DisplayObject()
	{
		Visible = Update = UpdateChild = HandleEvent = HandleEventChild = true;
		HandleEventMouse = HandleEventMouseNoHitTest = HandleEventMouseMove = 
			RemoveThis = m_mouse_inside = HandleEventMouseLock = false;

		User_Object = NULL;
		m_father	= NULL;
	}

	virtual ~DisplayObject(){}

	//DisplayObject as Container
	DisplayObjectPtr		GetFather(){return m_father->shared_from_this();}
	DisplayObjectPtr		GetChild(size_t idx){return m_container[idx];}
	size_t	GetChildCount()	{		return m_container.size();	}

	template<class T>
	shared_ptr<T> Add(shared_ptr<T> object){
		object->m_father = this;
		m_container.push_back(object);
		return object;
	}

	template<class T>
	shared_ptr<T> Add(T* object){
		object->m_father = this;
		shared_ptr<T> obj = shared_ptr<T>(object);
		m_container.push_back(obj);
		return obj;
	}

	int IndexOf(DisplayObjectPtr s){
		if(!m_container.empty())
		{
			int i = 0;
			DisplayObjectIt it = m_container.begin();
			for ( it=m_container.begin() ; it < m_container.end(); it++ )
			{
				if(s == (*it)) return i;
				i++;
			}

		}
		return -1;
	}
	void RemoveAll(){
		if(!m_container.empty())
		{
			DisplayObjectIt it = m_container.begin();
			for ( it=m_container.begin() ; it < m_container.end(); it++ )
			{

				(*it)->RemoveAll();
			}
			m_container.clear();
		}
	}
	void Remove(int idx){		m_container.erase(m_container.begin()+idx);}
	void Remove(DisplayObjectPtr s){	m_container.erase(m_container.begin()+IndexOf(s));}
	void RemoveSelf();
	virtual bool HitTest(GL_Vec2 abs){return false;}


	//Event
	bool OnEvent(SDL_Event* evt,int elapsed)
	{
		//if this sprite was told remove or not visible
		if(!Visible || !HandleEvent)	return false;

		//process update and remove
		if(!m_container.empty() && HandleEventChild)
		{
			size_t size = m_container.size();
			for ( size_t i = 0 ;i < size; )
			{
				DisplayObjectPtr temp = m_container[i];
				bool isHandleEvent = temp->OnEvent(evt,elapsed);
				if(HandleEventMouseLock && isHandleEvent)	break;

				++i;
			}

		}
		bool ret = false;

		//handle mouse event
		if(HandleEventMouse && m_listener.get() != NULL)
		{
			bool result = HandleEventMouseNoHitTest ? true : HitTest(GL_Vec2(evt->button.x,evt->button.y));
			if(result)
			{
				switch(evt->type)
				{
				case SDL_MOUSEMOTION:		ret |=	m_listener->OnMouseMove(evt,this);
					break;
				case SDL_MOUSEBUTTONDOWN:	ret |=	m_listener->OnMouseDown(evt,this);
					break;
				case SDL_MOUSEBUTTONUP:		ret |=	m_listener->OnMouseUp(evt,this);
					break;
				}

			} 

			
			if(HandleEventMouseMove && 
					m_mouse_inside != HitTest(GL_Vec2(evt->button.x,evt->button.y)))
			{
				ret |= m_listener->OnMouseOver(evt,this,m_mouse_inside);
				m_mouse_inside = !m_mouse_inside;
			}
			


		}
		return ret;
	}



	//Update function
	virtual void OnUpdateBase(int elapsed){}	//for interal use
	virtual void OnUpdate(int elapsed){}
	void OnUpdateChildren(int elapsed){
		if(elapsed == 0 || !Update) return;

		OnUpdateBase(elapsed);
		OnUpdate(elapsed);

		if(!UpdateChild) return;

		if(!m_container.empty())
		{
			//not uaw iterator because m_container may increase in this update loop
			for ( unsigned int i =0; i < m_container.size();i++  )
			{
				m_container[i]->OnUpdateChildren(elapsed);

			}

		}
	}

	///call back from main to prevent crach when update. 
	///this will be called automatically when needed.
	void CleanUpChildRemoved()
	{
		if(!m_container.empty())
		{
			DisplayObjectIt it = m_container.begin();
			for ( it=m_container.begin() ; it != m_container.end();  )
			{
				if((*it)->RemoveThis){
					it = m_container.erase(it);	continue;
				}
				(*it)->CleanUpChildRemoved();
				++it;
			}
		}
	}

	///set exact position of object at screen, ignore parent transformation
	void SetAbsPosition(float x,float y)
	{
		if(m_father != NULL)
		{
			Position.x = Position.x - m_father->GetAbsoluteX();
			Position.y = Position.y - m_father->GetAbsoluteY();
		}
		Position.x = x;
		Position.y = y;
	}
	///get exact position x in screen coordinate
	float GetAbsoluteX()
	{
		return (m_father != NULL) ? (m_father->GetAbsoluteX() + Position.x) : Position.x;

	}
	///get exact position y in screen coordinate
	float GetAbsoluteY()
	{
		return (m_father != NULL) ? (m_father->GetAbsoluteY() + Position.y) : Position.y;
	}
	///get exact position in screen coordinate
	GL_Vec2 GetAbsolute(){return (m_father != NULL) ? m_father->GetAbsolute() + Position : Position;}

	virtual void Draw(){};
	void DrawChildren()
	{
		if(!m_container.empty())
		{
			DisplayObjectIt it = m_container.begin();
			for ( it=m_container.begin() ; it < m_container.end(); it++ )
				(*it)->Draw();
		}
	}	


};
