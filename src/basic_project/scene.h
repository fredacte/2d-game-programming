#pragma once

class Scene
{
protected:
	virtual void OnSceneStart(){}	//start callback
	virtual Scene* OnSceneEnd(){}		//doing clean up
	virtual void OnStop(){}
	virtual void OnResume(){}
	virtual bool OnLoop(){}
	virtual void OnRender(){}
	virtual bool OnEvent(SDL_Event* evt){}


}