#include "console.h"
#include "basic_project_main.h"

Console* Console::m_instance = NULL;

Console::Console() : Sprite_GL()
{
	Max_Stack = 40;
	
}

shared_ptr<Console> Console::GetInstance()
{
	if(m_instance == NULL)
	{
		m_instance = new Console();
		m_instance->m_font = Basic_SDL::GL_AutoReleaseFont("Hobo.ttf",20);
		
		Basic_SDL::GetInstance()->GetScene()->Add(m_instance);

	}
	
	return dynamic_pointer_cast<Console>(m_instance->shared_from_this());
}

/// show text on screen. Max_Stack decides the total texts that can be shown.
void Console::Trace(std::string s)
{
	
	
	while(m_container.size() > Max_Stack)
	{
		this->Remove(0);
		
	}
	Sprite_Text* text = new Sprite_Text(s,GL_Color());
	text->ZOrder = 0.9f;
	text->TintColor = GL_Color(0,0,0,1);
	Add(text);
	for(unsigned int i = 0;i<m_container.size();i++)
	{
		m_container[i]->Position = GL_Vec2((i%2) != 0 ?text->GetWidth()+20.f:0.f,(i-i%2)*10.f);
	}
}