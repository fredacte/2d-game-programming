#pragma once

//	Author:			Cheung Kwok Wai
//	Description:	simple 2d math function
//

#include <SDL_Image.h>
#include <SDL_opengl.h>
#include <math.h>


class GL_Vec2{
public:
	float x;
	float y;

	GL_Vec2(float _x = 0,float _y = 0)
	{
		x = _x;
		y = _y;
	}
	static float Distance(GL_Vec2 p0,GL_Vec2 p1)
	{
		return sqrt((p0.x - p1.x)*(p0.x - p1.x) + (p0.y - p1.y)*(p0.y - p1.y));
	}
	static GL_Vec2 Normalize(GL_Vec2 p)
	{
		GL_Vec2 v0(0,0);
		float dist = Distance(v0,p);
		GL_Vec2 v;
		v.x = p.x / dist;
		v.y = p.y / dist;
		return v;
	}
	float GetMagnitude()
	{
		return sqrt(x*x + y*y);
	}

	GL_Vec2 operator=(const float &f)
	{
		x = f;
		y = f;
		return *this;
	}
	GL_Vec2 operator+(const GL_Vec2 &p) {
		return GL_Vec2(x+p.x,y+p.y);
	}

	GL_Vec2 operator-(const GL_Vec2 &p) {

		return GL_Vec2(x-p.x,y-p.y);
	}
	GL_Vec2 operator*(const float &f) {
		return GL_Vec2(x*f,y*f);
	}
	GL_Vec2 operator/(const float &f) {

		return GL_Vec2(x/f,y/f);
	}
	bool operator==(const GL_Vec2 &p){
		return p.x == x && p.y == y;
	}
};

/*

bool HitTestRect(int x0,int y0,int x1,int y1,
int w0,int h0,int w1,int h1)
{
return false;
//(abs(x0 - x1) * 2 < (w0 + w1)) &&
// (abs(y0 - y1) * 2 < (h0 + h1));


}
*/
typedef struct GL_Rect {
	float x, y;
	float w, h;
} GL_Rect;


typedef struct GL_Color {
	float red,green,blue,alpha;
	GL_Color(){red = green=blue=alpha=1;}
	GL_Color(float _r,float _g,float _b,float _a){red=_r;green=_g;blue=_b;alpha=_a;}

} GL_Color;


typedef struct GL_Colori {
	int r,g,b,a;
	GL_Colori(){r = g=b=a=255;}

} GL_Colori;

class Texture {
public:
	GLuint id;
	GLuint w;
	GLuint h;
	Texture()
	{
		id = w = h = 0;
	}
	Texture(GLuint _id,GLuint _w,GLuint _h){
		id = _id;
		w = _w;
		h = _h;
	}
	static Texture Load(SDL_Surface* surface)
	{
		GLenum texture_format;
		GLint nOfColors = surface->format->BytesPerPixel;
		if (nOfColors == 4)     // contains an alpha channel
		{
			if (surface->format->Rmask == 0x000000ff)
				texture_format = GL_RGBA;
			else
				texture_format = GL_BGRA;
		} else if (nOfColors == 3)     // no alpha channel
		{
			if (surface->format->Rmask == 0x000000ff)
				texture_format = GL_RGB;
			else
				texture_format = GL_BGR;
		} else {
			printf("warning: the image is not truecolor..  this will probably break\n");
			// this error should not go unhandled
		}
		GLuint texture = 0;
		glEnable( GL_TEXTURE_2D );
		// Have OpenGL generate a texture object handle for us
		glGenTextures( 1, &texture );

		// Bind the texture object
		glBindTexture( GL_TEXTURE_2D, texture );

		// Set the texture's stretching properties
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

		// Edit the texture object's image data using the information SDL_Surface gives us
		glTexImage2D( GL_TEXTURE_2D, 0, nOfColors, surface->w, surface->h, 0,
			texture_format, GL_UNSIGNED_BYTE, surface->pixels );

		return Texture(texture,surface->w,surface->h);
	}
	static Texture Load(std::string filename)
	{
		//Temporary storage for the image that's loaded
		SDL_Surface* loadedImage = NULL;

		//The optimized image that will be used
		SDL_Surface* optimizedImage = NULL;

		//Load the image (24 bits)
		loadedImage = IMG_Load( filename.c_str() );

		//If nothing went wrong in loading the image
		if( loadedImage != NULL )
		{

			optimizedImage = SDL_DisplayFormatAlpha( loadedImage );

			//Free the old image
			SDL_FreeSurface( loadedImage );
		}


		Texture t = Load(optimizedImage);

		SDL_FreeSurface(optimizedImage);

		return t;
	}
};