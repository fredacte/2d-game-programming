#include "sprite_test.h"

#include "example_sprite.h"
#include "..\sprite\sprite_gl.h"
#include <boost\format.hpp>

using namespace boost;

void sprite_test::Test_Basic()
{

	Sprite* sprite = new example_sprite(GL_AutoReleaseSurface("128x64.png"));
	Sprite* sprite2 = new example_sprite(GL_AutoReleaseSurface("128x128.png"));
	Sprite* sprite3 = new example_sprite(GL_AutoReleaseSurface("32x32.png"));

	sprite->SetPosition(100,100);
	sprite2->SetPosition(128,0);
	sprite->Add(sprite2);

	sprite3->SetPosition(128,64);
	sprite2->Add(sprite3);

	GL_AddSprite(sprite);
}
void sprite_test::Test_GLSprite()
{

	Sprite_GL* sprite = new Sprite_GL(GL_AutoReleaseSurface("128x64.png"),GL_AutoReleaseTexture("128x64.png"));
	Sprite_GL* sprite2 = new Sprite_GL(GL_AutoReleaseSurface("128x128.png"),GL_AutoReleaseTexture("128x128.png"));
	Sprite_GL* sprite3 = new Sprite_GL(GL_AutoReleaseSurface("32x32.png"),GL_AutoReleaseTexture("32x32.png"));

	sprite->SetCenter();
	sprite2->SetCenter();
	sprite3->SetCenter();

	sprite2->SetAlpha(0.5);
	sprite3->SetAlpha(0.8);
	sprite->SetPosition(100,100);
	sprite2->SetPosition(128,0);
	sprite->Add(sprite2);

	sprite3->SetPosition(128,64);
	sprite2->Add(sprite3);

	GL_AddSprite(sprite);
}
void sprite_test::Test_ModulePng()
{
	class Element : public Sprite_GL{

	public:
		enum Type{Water = 0,Fire,Metal,Wood,Earth,Empty};

		Element(SDL_Surface* surf,GLuint tex) : Sprite_GL(surf,tex)
		{	
			SplitFrames(6,1);
			this->m_eventMouse = true;
			this->SetCenter();
		}
		bool OnMouseDown(SDL_Event* evt)
		{
			this->SetNextFrame();
			Drag();
			return true;
		}
		void OnUpdate(int elapsed)
		{
			m_angle+= elapsed/ 10;
			static float scale = 1;
			static bool toggle = true;
			SetScale(scale);

			if(toggle)	scale-=0.01;else scale+=0.01;

			if(scale < 0 || scale > 1)	toggle = !toggle;
			
		}
		bool OnMouseUp(SDL_Event* evt)
		{
			Drop();
			return true;
		}
		void SetType(Type type)
		{
			m_type = type;
			SetCurrentFrame(m_type);
		}
		Type GetType(){return m_type;}
	private:
		Type m_type;
	};

	Element* spr = new Element(GL_AutoReleaseSurface("five_elements.png"),GL_AutoReleaseTexture("five_elements.png"));
	spr->SetType(Element::Type::Wood);

	GL_AddSprite(spr);
}
void sprite_test::Test_GLTexture()
{
	
	texture = this->GL_AutoReleaseTexture("five_elements.png");
	texture = this->GL_AutoReleaseTexture("five_elements.png");
	texture = this->GL_AutoReleaseTexture("five_elements.png");

}
bool sprite_test::OnInit()
{
	if(!Basic_SDL::OnInit()) return false;

	//Test_ModulePng();
	GL_Color cbk;
	cbk.r = cbk.g = cbk.b = 0.5;
	GL_SetBackgroundColor(cbk);

	Test_GLSprite();
	Test_ModulePng();
	
	return true;
}
void sprite_test::OnLoop()
{
	Basic_SDL::OnLoop();

}
void sprite_test::OnRender()
{






	
	GL_DrawRootSprite();

	/*
	std::string text = str(format("Sprite1 HitTest Sprite3: %1%") % sprite->HitTest(sprite3));
	this->GL_PrintText(text.c_str(),0,0,cWhite);

	int x0 = sprite->GetOriginX();
	int y0 = sprite->GetOriginY();
	int x1 = sprite3->GetOriginX();
	int y1 = sprite3->GetOriginY();


	int w0 = sprite->GetWidth();
	int h0 = sprite->GetHeight();
	int w1 = sprite3->GetWidth();
	int h1 = sprite3->GetHeight();

	this->GL_DrawBox(x0,y0,x0+w0,y0+h0,cWhite,pDisplaySurface);
	this->GL_DrawBox(x1,y1,x1+w1,y1+h1,cWhite,pDisplaySurface);
	*/

}
void sprite_test::OnEvent(SDL_Event* evt)
{

	Basic_SDL::OnEvent(evt);
}
void sprite_test::OnCleanup()
{

	Basic_SDL::OnCleanup();
}

int main(int argc, char *argv[])
{
	sprite_test sdl;
	return sdl.OnExecute();
}
