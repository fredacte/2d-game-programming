
#include "sprite_gl.h"
#include "..\basic_project\console.h"
//1.Sprite_GL* 2.SDL_Event* 3.elapsed
//return true means finished
bool Modifier::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	
	Sprite_GL* s = (Sprite_GL*)(a.get());
	return true;
}

bool ModifierMove::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	Sprite_GL* s = dynamic_cast<Sprite_GL*>(a.get());
	if(s == NULL) return true;

	

	
	if(!init)
	{
		
		GL_Vec2 offset = m_dest - s->Position;
		float distance = offset.GetMagnitude();
		 m_vOffset = GL_Vec2::Normalize(offset) * m_speed;
		 m_offset = m_vOffset.GetMagnitude();
		m_remain = distance;
		init = true;
		m_last_position = s->Position;

	}
	float t = elapsed / 1000.0f;

	if(m_last_position == s->Position)	{

		s->Position = s->Position + m_vOffset * t;
		m_last_position = s->Position;

		m_remain -= m_offset * t;
	}else{
		m_remain = -1;
	}

	
		
	

	if(m_remain < 0)
	{
		
		s->Position = m_dest;
			
			
		return true;
	}

	return false;
}

bool ModifierRemove::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	Sprite_GL* s = dynamic_cast<Sprite_GL*>(a.get());
	if(s == NULL) return true;

	m_time -= elapsed;
	if(m_time < 0)
	{
		s->RemoveSelf();
		return true;
	}
	return false;
	
}

bool ModifierAlpha::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	Sprite_GL* s = dynamic_cast<Sprite_GL*>(a.get());
	if(s == NULL) return true;

	m_timeElapsed += elapsed;

	float value_change;
	float percent = (float)m_timeElapsed / (float)m_timeTotal;
	value_change =  m_from_alpha *(1-percent) + m_to_alpha * percent;


	if(m_timeElapsed > m_timeTotal)
	{
		value_change = m_to_alpha;
		
	}

	ApplyChange(a,value_change);
	
	if(m_applyChild){
		ApplyChangeChild(a,value_change);
	}
	
	

	return value_change == m_to_alpha;

}
bool ModifierAlpha::ApplyChangeChild(DisplayObjectPtr a,float value)
{
	Sprite_GL* s = dynamic_cast<Sprite_GL*>(a.get());
	if(s == NULL) return true;

	size_t childCount = s->GetChildCount();

	for(size_t i = 0;i< childCount;i++)
	{
		DisplayObjectPtr ptr = s->GetChild(i);
		if(!ApplyChange(ptr,value))	
			//if convent to Sprite_GL fail, continue
			continue;
		ApplyChangeChild(ptr,value);

	}
	return true;
}
bool ModifierAlpha::ApplyChange(DisplayObjectPtr a,float value)
{
	Sprite_GL* s = dynamic_cast<Sprite_GL*>(a.get());
	if(s == NULL) return true;

	switch(m_type)
	{
	case Alpha:
		s->TintColor.alpha = value;
		break;
	case Scale:
		s->Scale = GL_Vec2(value,value);
		break;
	}
	return true;
}
bool ModifierWait::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	

	m_time -= elapsed;

	if(m_time < 0){
		
		if(m_msg != -1)
		{
			Sprite_GL* s = dynamic_cast<Sprite_GL*>(m_receiver.get());
			if(s != NULL)	s->ReceiveMessage(m_msg,m_userObject);
		}
		return true;
	}
	
	return false;
}




bool ModifierGroup::OnUpdate(DisplayObjectPtr a,int elapsed)
{
	bool finish = true;
	for(int i = 0;i<m_size;i++)	
	{
		if(!m_finsihed[i])
			m_finsihed[i] |= m_group[i]->OnUpdate(a,elapsed);
		finish &= m_finsihed[i];
	}

	return finish;
}
