#include "sprite_text.h"
#include "..\basic_project\console.h"
void Sprite_Text::UpdateText()
{
	//a bit slow
	Basic_SDL *sdl = Basic_SDL::GetInstance();
	Basic_SDL::GL_FreeTexture(&m_texture.id);

	SDL_Surface* surface = sdl->GL_CreateText(m_stext->c_str(),GL_Colori(),m_font);
	m_texture = Texture::Load(surface);
	SDL_FreeSurface(surface);

	SplitFrames(1,1);

}

Sprite_Text::Sprite_Text(string static_text,GL_Color c,const char* ffile,int ftsize ) : Sprite_GL()
{
	m_font = Basic_SDL::GL_AutoReleaseFont(ffile,ftsize);
	TintColor = c;
	m_stext = new string(static_text);
	UpdateText();
	m_is_dynamic = false;
	

}
Sprite_Text::~Sprite_Text(){
	
	Basic_SDL::GL_FreeTexture(&m_texture.id);
	if(m_stext != NULL)
		delete m_stext;
}
void Sprite_Text::Draw()
{
	if(m_is_dynamic)	UpdateText();
	Sprite_GL::Draw();
}
void Sprite_Text::SetText(string s)
{
	if(m_stext != NULL)	delete m_stext;
	m_stext = new string(s);
	UpdateText();
}
void Sprite_Text::SetTextPtr(string* sp)
{
	if(m_stext != NULL)	delete m_stext;
	m_stext = sp;
	UpdateText();
}
void Sprite_Text::SetDynamic(bool b)
{
	m_is_dynamic = b;
}
