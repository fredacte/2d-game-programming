#pragma once
#include "sprite.h"

class example_sprite : public Sprite
{
private:
	bool selected;
public:
	example_sprite(SDL_Surface* surface);
	
	bool OnMouseMove(SDL_Event* evt);
	bool OnMouseDown(SDL_Event* evt);
	bool OnMouseUp(SDL_Event* evt);
	void OnUpdate(int elapsed);

	
};