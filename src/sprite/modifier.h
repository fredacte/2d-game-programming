//	Author:			Cheung Kwok Wai
//	Description:	Modifier To Animate Sprite & Handle Event
//	Date:			26/3/2012

#pragma once
#include "..\basic_project\display_object.h"


class Modifier{
public:
	///return ture if continue
	///1.Sprite_GL* 2.SDL_Event* 3.elapsed
	virtual bool OnUpdate(DisplayObjectPtr a,int elapsed);

	///Caution: virtual destructor is important to prevent memory leak!!
	///this can allow devied class to call its destructor!!
	virtual ~Modifier(){}
};

class ModifierMove : public Modifier
{
public:
	GL_Vec2 m_dest;
	GL_Vec2 m_vOffset;
	GL_Vec2 m_last_position;

	float m_offset;
	float m_remain;
	float m_speed;

	bool init;

	ModifierMove(GL_Vec2 dest,float speed){
		m_dest = dest;
		m_speed = speed;
		init = false;
	}
	virtual bool OnUpdate(DisplayObjectPtr,int elapsed);
};

class ModifierRemove : public Modifier
{
public:
	int m_time;
	//in millisec
	ModifierRemove(int time){
		m_time = time;
	}

	bool OnUpdate(DisplayObjectPtr,int elapsed);
};

class ModifierAlpha: public Modifier
{
public:
	enum Type{Alpha,Scale};
	Type m_type;
	int m_timeTotal;
	int m_timeElapsed;
	float m_from_alpha;
	float m_to_alpha;
	bool m_applyChild;

	//in millisec
	ModifierAlpha(float from_alpha,float to_alpha,int time,Type t = Alpha,bool applyChild = false){
		m_timeTotal = time;
		m_from_alpha =from_alpha;
		m_to_alpha = to_alpha;
		m_timeElapsed = 0;
		m_type = t;
		m_applyChild = applyChild;
	}

	bool OnUpdate(DisplayObjectPtr,int elapsed);
	bool ApplyChange(DisplayObjectPtr,float value);
	bool ApplyChangeChild(DisplayObjectPtr,float value);
};

class ModifierWait : public Modifier
{
public:
	int m_time;
	void* m_userObject;
	int m_msg;
	DisplayObjectPtr m_receiver;
	ModifierWait(int time,int msg = -1,DisplayObjectPtr receiver = DisplayObjectPtr(),void* userObject = NULL){
		m_time = time;
		m_msg = msg;
		m_userObject = userObject;
		m_receiver = receiver;
	}
	virtual bool OnUpdate(DisplayObjectPtr,int elapsed);
};


class ModifierGroup : public Modifier
{
public:
	Modifier** m_group;
	bool* m_finsihed;
	int m_size;
	ModifierGroup(Modifier** group,int size)
	{
		m_group = group;
		m_size = size;
		m_finsihed = new bool[size];
		for(int i = 0;i<size;i++)
			m_finsihed[i] = false;
	}
	bool OnUpdate(DisplayObjectPtr,int elapsed);
	~ModifierGroup(){
		for(int i = 0;i<m_size;i++)
		{
			delete m_group[i];
		}
		delete[] m_group;
		delete[] m_finsihed;
	}

};

