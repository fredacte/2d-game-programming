//
// Subject:			SM2603 - 2D Game Programming
// Assignment:		1
// Student Name:	Cheung Kwok Wai
// Student Number:	52012205
// Date:			2/13/2012
//
// Assignment1.cpp - This is the skeletion file for Assignment 1
//
// requires static linkage to:
// sdl.lib, sdlmain.lib
//
// requires dynamic linkage to:
// sdl.dll
//

#include "..\basic_project\basic_project_main.h"

class sprite_test : public Basic_SDL
{
private:

GLuint texture;

public:
	sprite_test() : Basic_SDL(1024,768,true){}

    bool OnInit();
	void OnLoop();
	void OnRender();
	void OnEvent(SDL_Event* evt);
	void OnCleanup();

	void Test_Basic();
	void Test_ModulePng();
	void Test_GLTexture();
	void Test_GLSprite();

};

