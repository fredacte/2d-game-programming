#pragma once
#include "sprite_gl.h"
#include <boost\format.hpp>

using namespace boost;
class Sprite_Text;
typedef shared_ptr<Sprite_Text> SpriteTPtr;

class Sprite_Text : public Sprite_GL
{
private:
	string* m_stext;
	TTF_Font* m_font;
	bool m_is_dynamic;
	void UpdateText();
public:
	
	Sprite_Text(string ,GL_Color,const char* ffile = "Hobo.ttf",int ftsize = 20);
	~Sprite_Text();
	void Draw();
	
	string GetText(){return *m_stext;}
	string* GetTextPtr(){return m_stext;}
	void SetText(string s);
	void SetTextPtr(string* sp);
	void SetDynamic(bool b);
};