
#include "example_sprite.h"


example_sprite::example_sprite(SDL_Surface* surface) : Sprite(surface)
{
	this->m_eventMouse = true;
	this->m_eventMouseNoHitTest = false;
	selected = false;
	
	//this->SplitFrames(2,2);
}
bool example_sprite::OnMouseMove(SDL_Event* evt)
{
	if(selected)
	{
		this->SetAbsPosition(evt->button.x - this->GetWidth() / 2,
			evt->button.y - this->GetHeight() / 2);
		return true;
	}
	return false;
}
bool example_sprite::OnMouseDown(SDL_Event* evt)
{
	selected = true;
	SetUpdate(!GetUpdate());

	//this->SetNextFrame();
	//RemoveSelf();
	return true;
}
bool example_sprite::OnMouseUp(SDL_Event* evt)
{
	selected = false;
	
	return true;
}

void example_sprite::OnUpdate(int elapsed)
{
	
}

