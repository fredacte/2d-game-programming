
#include "sprite_gl.h"

//Sprite_GL
void Sprite_GL::Drag()
{
	m_drag = true;
}
void Sprite_GL::Drop()
{
	m_drag = false;
}

Sprite_GL::Sprite_GL() : DisplayObject()
{
	m_texture = Texture();

	m_frameRect = 0;
	m_frameRectFloat = 0;
	Rotation = 0;
	Scale	= 1.f;
	ZOrder = -0.99f;

	m_drag = false;
	
	SplitFrames(1,1);
	
	
}
Sprite_GL::Sprite_GL(std::string filename) : DisplayObject()
	{
		m_texture = Basic_SDL::GL_AutoReleaseTexture(filename);
		
		m_frameRect = 0;		//int rect for actual texture size in pixel
		m_frameRectFloat = 0;	//float rect for texture coordinate

		m_drag = false;

		Scale = 1.f;
		Rotation = 0;
		ZOrder = -0.99f;

		SplitFrames(1,1);
	}

int Sprite_GL::GetWidth(){return m_frameRect[m_currentFrame].w;}
int Sprite_GL::GetHeight(){return m_frameRect[m_currentFrame].h;}

void Sprite_GL::OnUpdateBase(int elapsed)
{
	//update modifier list
	if(!m_modiList.empty())
	{
		Modifier* modi = m_modiList.front().get();
		//if this modifier finished its process
		if(modi->OnUpdate(this->shared_from_this(),elapsed)){
			//erase this;
			OnModifierFinish(modi);
			m_modiList.erase(m_modiList.begin());
		}
	}
	

}
Sprite_GL::~Sprite_GL()
{
	delete[] m_frameRect;
	m_frameRect = 0;

	delete[] m_frameRectFloat;
	m_frameRectFloat = 0;
	m_modiList.clear();
}

bool Sprite_GL::OnEvent(SDL_Event* evt,int elapsed)
{
	if(!DisplayObject::OnEvent(evt, elapsed))		return false;

	// handle drag drop
	if(m_drag)
	{
		SetAbsPosition(evt->button.x,evt->button.y);
	}

	return true;
}

bool Sprite_GL::HitTest(GL_Vec2 abs)
{
	float ax = GetAbsoluteX()-Center.x;
	float ay = GetAbsoluteY()-Center.y;

	bool inX = abs.x >= ax && abs.x <= ax + GetWidth();

	if(!inX)	return false;
	bool inY = abs.y >= ay && abs.y <= ay+ GetHeight();

	return inY;
}



bool Sprite_GL::SplitFrames(int rows,int columns)
{

	if(rows <= 0 || columns <= 0)	return false;
	bool success = SetTotalFrames(rows*columns);
	if(!success)	return false;

	int width = m_texture.w / rows;
	int height = m_texture.h / columns;

	for(int i = 0;i< columns;i++)
	{
		for(int j = 0;j < rows;j++)
		{
			GL_Rect rectf;
			rectf.x = j * width / (float)m_texture.w;
			rectf.y = i * height / (float)m_texture.h;
			rectf.w = rectf.x + width / (float)m_texture.w;
			rectf.h = rectf.y + height / (float)m_texture.h;

			m_frameRectFloat[j+i*rows] = rectf;

			SDL_Rect rect;
			rect.x = j * width;
			rect.y = i * height;
			rect.w = width;
			rect.h = height;

			m_frameRect[j+i*rows] = rect;
		}
	}
	return true;

}
bool Sprite_GL::SetTotalFrames(int total)
{


	if(total >=1)
	{	//if exist, remove old
		if(m_frameRectFloat != NULL)
			delete[] m_frameRectFloat;
		if(m_frameRect != NULL)
			delete[] m_frameRect;

		m_frameRect = new SDL_Rect[total];
		m_frameRectFloat = new GL_Rect[total];

		
		m_currentFrame = 0;
		m_maxFrames = total;
		return true;

	}
	return false;
}
void Sprite_GL::SetNextFrame()
{
	m_currentFrame++;
	if(m_currentFrame == m_maxFrames)
		m_currentFrame = 0;

}
void Sprite_GL::SetCurrentFrame(int idx){
	if(idx>=0 && idx < m_maxFrames)		m_currentFrame = idx;
}

int Sprite_GL::GetCurrentFrame(){return m_currentFrame;}
int Sprite_GL::GetTotalFrames(){return m_maxFrames;}
void Sprite_GL::Draw()
{
	if(m_father ==0 || !Visible)
		return;
	
	if(m_texture.id == 0){
		glPushMatrix();
		glTranslatef(Position.x,Position.y,0);
		DrawChildren();
		glPopMatrix();
		return;
	}

	
	glPushMatrix();
	glBindTexture( GL_TEXTURE_2D, m_texture.id );
	glTranslatef(Position.x,Position.y,0);
	
	
	//push matrix before rotation to prevent affect children node's rotation
	glPushMatrix();

	//scale rotation
	glScalef(Scale.x,Scale.y,1);
	glRotatef(Rotation,0,0,1);
	//m_angle++;

	//center point & z-order
	glTranslatef(-Center.x,-Center.y,ZOrder);
	
	//full brightness
	glColor4f(TintColor.red,TintColor.green,TintColor.blue,TintColor.alpha);         
	//blending function for translucency based on source alpha 
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);       
	

	glBegin( GL_QUADS );
	//bottom-left vertex (corner)
	GL_Rect rect = m_frameRectFloat[m_currentFrame];

	glTexCoord2f(rect.x, rect.y );
	glVertex3i( 0, 0, 0 );

	//bottom-right vertex (corner)
	glTexCoord2f( rect.w, rect.y );
	glVertex3i( GetWidth(),0, 0 );

	//top-right vertex (corner)
	glTexCoord2f( rect.w,   rect.h );
	glVertex3i(  GetWidth(),  GetHeight(), 0 );

	//top-left vertex (corner)
	glTexCoord2f( rect.x,  rect.h  );
	glVertex3i( 0, GetHeight(), 0 );
	glEnd();

	//pop matrix but still keep the center offset
	glPopMatrix();
	//glTranslatef(-m_xCenter,-m_yCenter,0);

	DrawChildren();
	glPopMatrix();
}