//	Author:			Cheung Kwok Wai
//	Description:	Use OpenGL to support Scale,Rotate,Alpha and ZOrder
//	Date:			26/3/2012

#pragma once
#include "modifier.h"
#include <SDL_opengl.h>
#include "..\basic_project\basic_project_main.h"


class Sprite_GL;

typedef shared_ptr<Sprite_GL> SpritePtr;


class Sprite_GL : public DisplayObject
{

protected:
	Texture m_texture;

	SDL_Rect* m_frameRect;
	GL_Rect* m_frameRectFloat;

	int m_currentFrame;
	int m_maxFrames;

	bool m_drag;
	//modifier list for animation
	vector<shared_ptr<Modifier>> m_modiList;
	
	virtual bool SetTotalFrames(int total = 1);
public:
	GLfloat Rotation;
	GL_Vec2 Scale;
	GLfloat ZOrder;
	GL_Color TintColor;

	int GetWidth();
	int GetHeight();
	void SetCenter(){Center = GL_Vec2(GetWidth() / 2.f,GetHeight() / 2.f);}

	Sprite_GL();
	Sprite_GL(std::string filename);
	virtual ~Sprite_GL();
	virtual void Draw();

	virtual void ReceiveMessage(int msg,void* user_object){}

	//Collision
	bool HitTest(GL_Vec2 abs);
	
	//Frame Setting
	void SetNextFrame();
	void SetCurrentFrame(int idx);
	int GetCurrentFrame();
	int GetTotalFrames();
	virtual bool SplitFrames(int rows,int columns);


	//Event 
	virtual bool OnEvent(SDL_Event* evt,int elapsed);
	virtual void OnUpdate(int elapsed){}
	void Drag();
	void Drop();

	//Modifier List
	void AddModifier(Modifier* m){m_modiList.push_back(shared_ptr<Modifier>(m));}
	void OnUpdateBase(int elapsed);
	virtual void OnModifierFinish(Modifier* m){}


	//friend class Modifier;
};