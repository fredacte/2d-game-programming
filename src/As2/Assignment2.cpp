//
//	Subject:		SM2603 - 2D Game Programming
//	Assignment:		2
//	Student Name:	Cheung Kwok Wai
//	Student Number: 52012205
//	Date:			10/3/2012
//
//  Filename: Assignment2.cpp
//
//  Note: This example is based on Bill Kendrick's Chicken Game Example
//
//	Program Description:
//
//		This C++/SDL code simulate a classic frogger game with a chicken!
//		.... put in your own description here ....
//
//

#include "Assignment2.h"
#include "Sprite_Car.h"
#include "Sprite_Chicken.h"
#include "Sprite_Question.h"
#include <boost\format.hpp>

using namespace boost;


Sprite* Assignemnt2::CreateCarList(int num = 9,int base_offset = 32,int line_offset = 64,bool invert_direction = true)
{
	Sprite* spr = new Sprite(NULL);

	//ignore first row
	for( int i = 1;i<num;i++)
	{
		//Create Car Sprite
		Sprite* tempCar = GL_CreateSprite<Sprite_Car>(&m_surCar,"car.png");
		tempCar->SetY(i*line_offset + base_offset);
		tempCar->SetX(i*line_offset);

		if(invert_direction)    ((Sprite_Car*)tempCar)->SetDirection(i%2 == 0);

		spr->Add(tempCar);

	}
	return spr;

}

Sprite* Assignemnt2::CreateChicken(int x,int y)
{

	Sprite_Chicken* spr = GL_CreateSprite<Sprite_Chicken>(&m_surChicken,"chicken.png");
	Sprite* spr2 = GL_CreateSprite<Sprite>(&m_surSelector,"selector_32px.png");
	//spr2->SetCenter(m_surSelector->w / 2,m_surSelector->h / 2);
	spr2->SetPosition(-m_surSelector->w / 2,-m_surSelector->h / 2);
	spr->SetSelector(spr2);
	spr->SetPosition(x,y);

	return spr;


}

Sprite* Assignemnt2::CreateEgg(int x,int y)
{
	Sprite* spr = GL_CreateSprite<Sprite>(&m_surEgg,"egg.png");
	spr->SetCenter(spr->GetWidth() / 2,spr->GetHeight() / 2);
	spr->SetPosition(x,y);
	return spr;

}

void Assignemnt2::SetupGame()
{
	//Create Background
	m_spiBK = GL_CreateSprite<Sprite>(&m_surBK,"road.png");
	GL_AddSprite(m_spiBK);

	//Create Egg Container
	m_spiEggs = new Sprite(NULL);
	GL_AddSprite(m_spiEggs);

	//Create Car List
	m_spiCars = CreateCarList();
	GL_AddSprite(m_spiCars);

	//Create Chickens
	m_spiChickens = new Sprite(NULL);
	m_totalChickens = 10;
	for(int i = 0;i < m_totalChickens;i++)
	{
		Sprite* chicken = CreateChicken(GetScreenWidth() / 2 - (m_totalChickens*64/2) + i * 64,GetScreenHeight() - 32);
		m_spiChickens->Add(chicken);
	}
	GL_AddSprite(m_spiChickens);



	//Create UI
	m_uiQuestion = GL_CreateSprite<Sprite_Question>(&m_surQuestion,"ui_question.png");
	m_uiQuestion->SetPosition(GetScreenWidth() - m_uiQuestion->GetWidth(),GetScreenHeight() - m_uiQuestion->GetHeight());
	GL_AddSprite(m_uiQuestion);
}

void Assignemnt2::CleanUpGame()
{


	this->GetScene()->RemoveAll();

	m_spiCars = 0;
	m_spiChickens = 0;
	m_spiBK = 0;
	m_spiEggs=0;
	m_uiQuestion = 0;

	Sprite_Chicken::arrived_chickens = 0;
	Sprite_Chicken::dead_chickens = 0;
	Sprite_Chicken::value_total = 0;

}
bool Assignemnt2::OnInit()
{
	if(!Basic_SDL::OnInit()) return false;
	SetupGame();
	//Show help to player.
	((Sprite_Question*)(m_uiQuestion))->SetEnable(true);

	return true;
}
void Assignemnt2::Pause()
{
	m_spiChickens->SetEventMouseChild(false);
	m_spiCars->SetVisible(false);
	m_spiEggs->SetVisible(false);
	GetScene()->SetUpdate(false);
}
void Assignemnt2::Resume()
{
	m_spiChickens->SetEventMouseChild(true);
	m_spiCars->SetVisible(true);
	m_spiEggs->SetVisible(true);
	GetScene()->SetUpdate(true);
}
void Assignemnt2::OnLoop()
{
	Basic_SDL::OnLoop();

	if(((Sprite_Question*)m_uiQuestion)->GetEnable())
	{
		Pause();
		return;
	}else
	{
		Resume();
	}

	if(GameFinished())
	{
		Pause();
		return;
	}

	
	//check car to chicken , chicken to egg collision
	vector<Sprite*> vtCarList = m_spiCars->GetContainer();
	vector<Sprite*> vtChickenList = m_spiChickens->GetContainer();
	vector<Sprite*> vtEggs = m_spiEggs->GetContainer();

	if(!vtCarList.empty() && !vtChickenList.empty())
	{

		vector<Sprite*>::iterator it = vtChickenList.begin();
		for ( it=vtChickenList.begin() ; it < vtChickenList.end(); it++ )
		{
			// get real chicken position with center point offset
			int cx = (*it)->GetX();
			int cy = (*it)->GetY();
			//Chicken to Car
			vector<Sprite*>::iterator it2 = vtCarList.begin();
			for ( it2=vtCarList.begin() ; it2 < vtCarList.end(); it2++ ){
				//Point hit test to chicken sprite
				if((*it2)->HitTest(cx,cy))
				{
					//Callback to car and chicken, let them response what to do.
					((Sprite_Car*)(*it2))->OnChickenHit((*it));
					((Sprite_Chicken*)(*it))->OnCarHit(*it2);
				}
			}
			//Chicken to Egg
			vector<Sprite*>::iterator it3 = vtEggs.begin();
			for ( it3=vtEggs.begin() ; it3 < vtEggs.end();it3++ )
			{
				if((*it3)->HitTest(cx,cy))
				{
					if(!(*it3)->m_removeLastUpdate)
						((Sprite_Chicken*)(*it))->OnEggHit((*it3));

					(*it3)->RemoveSelf();


				}

			}


		}
		
	}

	//check selected chicken to egg collision


	//spawn egg
	m_spawnEggTimer+= GetLastTimeElapsed();
	if(m_spawnEggTimer > m_spawnEggTimeMax)
	{
		//create an egg in screen
		m_spiEggs->Add(CreateEgg(rand() % (GetScreenWidth() - 150) + 75,
			rand() % (GetScreenHeight() - 150) + 75));

		m_spawnEggTimer = 0;
	}

}
int Assignemnt2::GetScore()
{
	return (Sprite_Chicken::value_total) - (GetTotalTimeElapsed() / 1000) * 2 + 100;
}
bool Assignemnt2::GameFinished()
{
	return (Sprite_Chicken::arrived_chickens + Sprite_Chicken::dead_chickens) == m_totalChickens || GetScore() <=0;
}
void Assignemnt2::OnEvent(SDL_Event* evt)
{
	Basic_SDL::OnEvent(evt);

	if(evt->button.button == SDL_BUTTON_RIGHT && 
		Sprite_Chicken::selected_chicken != NULL)
	{
			GL_Vec2 mouse_pos(evt->button.x,evt->button.y);
			Sprite_Chicken::selected_chicken->MoveTo(mouse_pos);
	}else if(evt->key.keysym.sym == SDLK_RETURN && 
		GameFinished())
	{
		Pause();
		CleanUpGame();
		Reset();
		SetupGame();
		Resume();
	}
}
void Assignemnt2::OnRender()
{
	Basic_SDL::OnRender();
	//static Uint32 cBackground = GL_CreateColor(100,100,100);
	//GL_DrawBackground(cBackground);								// draw background
	GL_DrawRootSprite();

	//finally , draw the ui.
	DrawUI();

}
void Assignemnt2::DrawUI()
{
	SDL_Surface* main_display = GL_GetMainDisplay();

	static int w2 = GetScreenWidth() / 2;
	static int h2 = GetScreenHeight() / 2;
	static int color_white = GL_CreateColor(255,255,255);

	std::string newText;
	if(((Sprite_Question*)m_uiQuestion)->GetEnable())
	{
		static int yOffset = 50;
		newText =  str(format("Select And Order Chickens To Cross All The Roads"));
		GL_PrintText(newText.c_str(),w2,yOffset,color_white,true,false);

		newText = str(format("Game Will End When Score Becomes To Zero."));
		GL_PrintText(newText.c_str(),w2,20+yOffset,color_white,true,false);

		newText = str(format("When A Chicken Collects A Gold Egg"));
		GL_PrintText(newText.c_str(),w2,140+yOffset,color_white,true,true);
		newText = str(format("It Will Become Faster ,Stronger And More Valuable."));
		GL_PrintText(newText.c_str(),w2,160+yOffset,color_white,true,true);

		newText = str(format("Also Verterns Worths More.They Don't Afraid Of Car."));
		GL_PrintText(newText.c_str(),w2,270+yOffset,color_white,true,true);
		newText = str(format("And Can Stay At A Place Instead Of Wandering."));
		GL_PrintText(newText.c_str(),w2,290+yOffset,color_white,true,true);


		newText = str(format("Game Pause"));
		GL_PrintText(newText.c_str(),w2,450+yOffset,color_white,true,true);

		newText = str(format("Click Question Button To Continue"));
		GL_PrintText(newText.c_str(),w2,500+yOffset,color_white,true,true);

		


	}else if(!GameFinished())
	{
		
		newText = str(format("Score: %1% ") % GetScore());
		GL_PrintText(newText.c_str(),w2 + 200,0,color_white,true,false);

		if(Sprite_Chicken::selected_chicken != NULL){
			newText = str(format("Selected Chicken Value: %1% ") % (Sprite_Chicken::selected_chicken->GetCurrentValue()));
			GL_PrintText(newText.c_str(),w2 -150,0,color_white,false,false);
		}
		//print text
		newText =  str(format("Arrvied:%1%") % Sprite_Chicken::arrived_chickens);
		GL_PrintText(newText.c_str(),0,0,GL_CreateColor(255,255,255));

		newText =  str(format("Dead:%1%") % Sprite_Chicken::dead_chickens);
		GL_PrintText(newText.c_str(),100,0,GL_CreateColor(255,255,255));

		static int time_text_max_width = 0;
		//fin max width to prevent sudden change

		newText =  str(format("Timer:%1%") % (GetTotalTimeElapsed() / 1000));
		SDL_Surface* text = GL_CreateText(newText.c_str(),color_white);
		time_text_max_width = text->w > time_text_max_width ? text->w : time_text_max_width;
		GL_ApplySurface(GetScreenWidth() - time_text_max_width,0,text,main_display);
		GL_FreeSurface(text);
	}
	else
	{
		newText = str(format("Your Score: %1% ") % GetScore());
		GL_PrintText(newText.c_str(),w2,h2,color_white,true,false);


		newText = str(format("Press Enter To Restart")); 
		GL_PrintText(newText.c_str(),w2,h2 + 32,color_white,true,false);
	}

}
void Assignemnt2::OnCleanup()
{
	GL_FreeSurface(m_surCar);
	GL_FreeSurface(m_surChicken);
	GL_FreeSurface(m_surBK);
	GL_FreeSurface(m_surSelector);
	GL_FreeSurface(m_surQuestion);
	GL_FreeSurface(m_surEgg);

	Basic_SDL::OnCleanup();
}

int main(int argc, char *argv[])
{
	Assignemnt2 sdl;
	return sdl.OnExecute();
}

