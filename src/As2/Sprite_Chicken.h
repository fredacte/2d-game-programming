#pragma once
#include "..\sprite\sprite.h"

#include "..\basic_project\GL_Math.h"

class Sprite_Chicken : public Sprite
{
private:
	int m_speed;
	GL_Vec2 m_pTarget;
	GL_Vec2 m_vOffset;
	float m_offset;
	float m_remain;

	bool m_walking;
	bool m_beHit;

	//Animation
	bool m_flipAnimation;

	//Gameplay
	int m_timeElapsed;
	int m_timeStay;
	Sprite* m_spiSelector;
	bool m_arrvied;
	int m_hp;
	float m_walkedDistance;
public:
	static Sprite_Chicken* selected_chicken;
	static int arrived_chickens;
	static int dead_chickens;
	static int value_total;
	Sprite_Chicken(SDL_Surface* surface);

	//bool OnMouseDown(SDL_Event* evt);
	bool OnMouseUp(SDL_Event* evt);
	void OnUpdate(int elapsed);
	int GetHp(){	return m_hp;}
	int GetCurrentValue();
	void OnCarHit(Sprite* car);
	void OnEggHit(Sprite* egg);
	void MoveTo(GL_Vec2 v);
	void SetSelector(Sprite* selector);
	void SetSelection(bool isSelect);
	bool GetSelection();
	void Wander();

protected:
	void UpdateFrame(int elapsed);

};
