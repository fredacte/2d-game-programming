#include "Sprite_Question.h"


Sprite_Question::Sprite_Question(SDL_Surface* surface) : Sprite(surface)
{
	this->m_eventMouse = true;
	m_enable = false;
	
}

bool Sprite_Question::OnMouseDown(SDL_Event* evt)
{
	m_enable = !m_enable;

	return true;
}


