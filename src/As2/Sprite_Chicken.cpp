
#include "Sprite_Chicken.h"
Sprite_Chicken* Sprite_Chicken::selected_chicken = NULL;
int Sprite_Chicken::arrived_chickens = 0;
int Sprite_Chicken::dead_chickens = 0;
int Sprite_Chicken::value_total = 0;

Sprite_Chicken::Sprite_Chicken(SDL_Surface* chicken) : Sprite(chicken)
{
	m_eventMouse= true;
	m_eventMouseNoHitTest = false;
	
	m_walking = false;
	m_speed = 2;
	m_flipAnimation = false;
	m_timeElapsed = 0;
	m_spiSelector = 0;
	m_beHit = false;
	m_arrvied = false;
	m_walkedDistance = 0;

	m_hp = 1;

	SetUpdate(true);
	SplitFrames(5,5);
	SetCenter(GetWidth()/2,GetHeight()/2);

	m_timeStay = 0;

}

void Sprite_Chicken::SetSelector(Sprite* selector)
{
    m_spiSelector = selector;
    Add(m_spiSelector);
	SetSelection(false);
}
void Sprite_Chicken::SetSelection(bool isSelect)
{
    if(m_spiSelector != NULL)
	{
        m_spiSelector->SetVisible(isSelect);
		if(isSelect){
			//diselect last selected chicken
			if(selected_chicken != NULL && selected_chicken != this)
				selected_chicken->SetSelection(false);
			selected_chicken = this;
		}
	}
}
bool Sprite_Chicken::GetSelection()
{
    if(m_spiSelector != NULL)
        return m_spiSelector->GetVisible();
	else
		return false;
}
bool Sprite_Chicken::OnMouseUp(SDL_Event* evt)
{
	if(evt->button.button == SDL_BUTTON_LEFT)
	{
		SetSelection(true);
		return true;
	}
	return false;
}

void Sprite_Chicken::MoveTo(GL_Vec2 v)
{
	if(m_arrvied)	return;
	m_walking = true;


	GL_Vec2 current(GetX(),GetY());
	GL_Vec2 offset = v - current;

	float distance = offset.GetMagnitude();

	m_vOffset = GL_Vec2::Normalize(offset) * m_speed;

	m_offset = m_vOffset.GetMagnitude();

	m_remain = distance;


}
void Sprite_Chicken::OnUpdate(int elapsed)
{
	UpdateFrame(elapsed);

	m_speed = m_hp * 2;
	if(m_speed == 0)
		m_speed = 1;

	if(m_walking)
	{
		m_x+= m_vOffset.x;
		m_y+= m_vOffset.y;

		m_remain -= m_offset;
		m_walkedDistance += m_offset;

		if(m_remain < 0 ){
			m_walking = false;

		}
		m_timeStay = 0;

	}else if(m_hp < 5)	//if hp more than 4 , chicken will not wander and keep stay at a place
	{
		//if stay to long, chichen will wander
		m_timeStay+= elapsed;
		if(rand() % (100*m_hp) == 0)	//change to random
		{
			Wander();
			m_timeStay = 0;
		}
	}

	//move uppder and remove
	if(m_y < 32 && !m_arrvied)
	{
		
		m_eventMouse=false;
		GL_Vec2 newPos(GetX(),m_y-100);
		MoveTo(newPos);
		m_arrvied = true;
		arrived_chickens++;

		//add value in terms of hp and walked distance
		value_total+=GetCurrentValue();
	}

	if(m_arrvied && m_y < -32)
	{
		if(selected_chicken == this)	selected_chicken = NULL;

		this->RemoveSelf();
	}

	
}
int Sprite_Chicken::GetCurrentValue()
{	
	return m_hp*50 + m_walkedDistance / 100;
}
void Sprite_Chicken::Wander()
{
	GL_Vec2 vOffset(rand() % 32 - 16,rand() % 32 - 16);
	GL_Vec2 current(GetX(),GetY());
	GL_Vec2 newpos = current + vOffset;
	newpos.x = max<float>(min<float>(800 - 20,newpos.x),20);
	newpos.y = max<float>(min<float>(640 - 20,newpos.y),20);
	MoveTo(newpos);

}

void Sprite_Chicken::UpdateFrame(int elapsed)
{
	m_timeElapsed += elapsed;
	int animationBase = m_hp;
	animationBase = max(min(5,animationBase),1);
	animationBase = (animationBase - 1) * 5;
	

	if(m_timeElapsed > 400 / m_speed){

		m_timeElapsed = 0;
		m_flipAnimation = !m_flipAnimation;
		m_beHit = false;
	}
	if(m_beHit)
	{
		SetCurrentFrame(animationBase+4);
	}else{
		if(m_vOffset.x > 0)
		{
			SetCurrentFrame(m_walking ? (m_flipAnimation ? animationBase+1 : animationBase) : animationBase);
		}else{
			SetCurrentFrame(m_walking ? (m_flipAnimation ? animationBase+3 : animationBase+2) : animationBase+2);
		}
	}
}

void Sprite_Chicken::OnCarHit(Sprite* car)
{
	// get far away from car/ up or down
	GL_Vec2 car_pos(car->GetX(),car->GetY());
	GL_Vec2 chicken_pos(GetX(),GetY());
	GL_Vec2 vOffset = chicken_pos-car_pos+chicken_pos;
	//GL_Vec2 new_pos =
	MoveTo(vOffset);
	m_beHit = true;

	m_hp--;
	if(m_hp == 0)
	{
		dead_chickens++;
		if(selected_chicken == this)	selected_chicken = NULL;
		this->RemoveSelf();
	}

}

void Sprite_Chicken::OnEggHit(Sprite* egg)
{
	m_hp++;

	
}
