#include "..\basic_project\basic_project_main.h"


class Assignemnt2 : public Basic_SDL
{
private:
    SDL_Surface* m_surCar;
	SDL_Surface* m_surChicken;
	SDL_Surface* m_surBK;
	SDL_Surface* m_surSelector;
	SDL_Surface* m_surQuestion;
	SDL_Surface* m_surEgg;

	Sprite* m_spiCars;
	Sprite* m_spiChickens;
	Sprite* m_spiBK;
	Sprite* m_uiQuestion;
	Sprite* m_spiEggs;


	int m_totalChickens;
	int m_spawnEggTimer;
	int m_spawnEggTimeMax;

public:
	Assignemnt2() : Basic_SDL(800,640)
	{
		m_surCar = 0;
		m_surChicken = 0;
		m_surBK = 0;
		m_surSelector = 0;
		m_surQuestion=0;
		m_surEgg = 0;
		m_spiCars = 0;
		m_spiChickens = 0;
		m_spiBK = 0;
		m_totalChickens = 0;

		m_spawnEggTimeMax = 4000;
		m_spawnEggTimer = 0;
	}

    bool OnInit();
	void OnRender();
	void OnCleanup();
	void OnLoop();
	void OnEvent(SDL_Event* evt);
	bool GameFinished();		//return true when game end

	//Game Flow Control
	void SetupGame();
	void CleanUpGame();
	void Pause();
	void Resume();
	void DrawUI();
	int GetScore();

	//Factory method
	Sprite* CreateCarList(int num,int base_offset,int line_offset,bool invert_direction);
	Sprite* CreateChicken(int x,int y);
	Sprite* CreateEgg(int x,int y);


};

