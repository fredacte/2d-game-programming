#pragma once
#include "..\sprite\sprite.h"

class Sprite_Car : public Sprite
{
private:
	int m_speed;
	bool m_toRight;
public:
	Sprite_Car(SDL_Surface* surface);
	
	void OnUpdate(int elapsed);
	void SetSpeed(int speed){ m_speed = speed;}
	int GetSpeed(){return m_speed;}
	void SetDirection(bool toRight)
	{	
		m_toRight = toRight;
		if(m_toRight){
			this->SetCurrentFrame(1);
		}else{
			this->SetCurrentFrame(0);
		}
	}
	bool GetDirection(){return m_toRight;} 
	void OnChickenHit(Sprite* chicken);
	void RandomSpeedSide();
	void UpdateAnimation();

	
};