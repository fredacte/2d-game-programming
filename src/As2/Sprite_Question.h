#include "..\sprite\sprite.h"

class Sprite_Question : public Sprite
{
protected:
	bool m_enable;
public:
	Sprite_Question(SDL_Surface* surface);
	
	bool OnMouseDown(SDL_Event* evt);
	bool GetEnable(){return m_enable;}
	void SetEnable(bool b){m_enable = b;}

	
};