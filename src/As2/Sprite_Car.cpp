#include "Sprite_Car.h"
#include <time.h>

Sprite_Car::Sprite_Car(SDL_Surface* surface) : Sprite(surface)
{
	m_update = true;
		
	m_speed = 2;
	//RandomSpeedSide();

	//Split to animation
	SplitFrames(1,6);

	SetCurrentFrame(1);
	SetCenter(GetWidth()/2,GetHeight()/2);

}
void Sprite_Car::OnUpdate(int elapsed)
{
	

	//apply movement
	m_x+= m_toRight ? m_speed : -m_speed ;

	//clamp to screen
	bool side_change = false;

	if(m_x > 800)
	{
		m_x = - this->GetWidth() + 1;
		side_change = true;
		
	}
	else if(m_x < -this->GetWidth())
	{
		m_x = 800;
		side_change = true;
	}

	if(side_change){
		RandomSpeedSide();
	}
}
void Sprite_Car::OnChickenHit(Sprite* chicken)
{
	//m_toRight = !m_toRight;
}
void Sprite_Car::RandomSpeedSide()
{
	//srand(time(NULL));
	m_speed = rand() % 9+1;
	m_toRight = rand() % 2 == 0;

	//update animation
	UpdateAnimation();
}

void Sprite_Car::UpdateAnimation()
{
	if(m_speed < 4)
	{
		SetCurrentFrame(m_toRight ? 1 : 0);
	}else if(m_speed < 6){
		SetCurrentFrame(m_toRight ? 2 : 3);
	}else if(m_speed < 10){
		SetCurrentFrame(m_toRight ? 4 : 5);
	}
	
}