
#pragma once

#include "five_element.h"
#include "..\basic_project\basic_project_header.h"

Five_element::Five_element() : Basic_SDL(1024,768,true)
{

}
void Five_element::StartGame()
{
	if(m_panel.get() != NULL)
	{
		m_panel->RemoveSelf();
	}
	m_panel = GetScene()->Add(new Panel());
	m_panel->Start();

	m_gs = Game;
}

void Five_element::StartGame(Level_Setting* ls )
{
	if(m_panel.get() != NULL)
	{
		m_panel->RemoveSelf();
	}
	m_panel = GetScene()->Add(new Panel(ls));
	m_panel->Start();
	
	m_gs = Game;
}


void Five_element::StartLevelMenu()
{
	if(m_menu_level.get() != NULL)
	{
		m_menu_level->Show();
	}else{
		m_menu_level = GetScene()->Add(new Menu_Level());
		m_menu_level->Show();
		
	}
	m_gs = LevelMenu;
	
}

void Five_element::ShowMenu()
{
	m_menu->Show();
	m_gs = MainMenu;
}
bool Five_element::OnInit()
{
	if(!Basic_SDL::OnInit()) return false;
	SDL_WM_SetCaption("Five Elements", "Five Elements");

	GL_SetBackgroundColor(GL_Color(0.5f,0.5f,0.5f,1.0f));

	//Background
	m_bk = GetScene()->Add(new Sprite_GL("bk.png"));
	m_bk->ZOrder = -0.9f;

	//Menu
	m_menu = GetScene()->Add(new Menu());
	m_menu->ToggleMenu();
	m_gs = MainMenu;

	return true;

}
void Five_element::OnEvent(SDL_Event* evt)
{


	Basic_SDL::OnEvent(evt);
	if(KEY_PRESSED(evt,SDLK_ESCAPE))	{
		switch(m_gs)
		{
		case LevelMenu:
			m_menu->Show();
			m_menu_level->Hide();
			m_gs = MainMenu;
		break;
		case Game:
			m_menu->Show();
			m_panel->Hide();
			m_gs = MainMenu;
		break;
		case MainMenu:
			if(m_panel.get() != NULL)
			{
				m_menu->Hide();
				m_panel->Show();
				m_gs = Game;
			}

		}
	}
}
