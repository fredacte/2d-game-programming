#pragma once
#include "..\sprite\sprite_text.h"
#include "element.h"

class Menu_Button : public Sprite_GL{
private:
	class Handler : public EventListener{
		
	public:
		float Speed;
		SpriteTPtr Name;
		Handler(){	Speed = 10;}
		bool OnMouseMove(SDL_Event *evt,DisplayObject* obj);
		bool OnMouseOver(SDL_Event* evt,DisplayObject* obj,bool isEndTouch);
		bool OnMouseDown(SDL_Event* evt,DisplayObject* obj);
		virtual ~Handler(){ 
			Name.reset();	
			//Add function can not assure this text sprite to be GC. so reset it explicitly.
			//
		}
	};
	friend class Handler;
	Handler* handler;
public:

	

	enum Event{OnHideTextFinished = 1,OnShowTextFinished};
	Menu_Button()	: Sprite_GL("yin_yang.png")
	{
		
		this->SetCenter();
		SetEventMouse(true,true,true);
		handler = new Handler();
		SetEventListener(handler);

	}
	void ReceiveMessage(int msg,void *user_object)
	{
		if(msg == OnHideTextFinished){
			handler->Name->Visible = false;
		}else if(msg == OnShowTextFinished)
		{

		}
	}
	void OnUpdate(int elapsed)
	{

		Rotation += (float)elapsed / 1000.0f * handler->Speed;
		if(Rotation > 360.f)
			Rotation -= 360.f;
		this->Scale = 0.8f+handler->Speed/500.f;

	}
	


};

struct Level_Setting;
typedef vector<shared_ptr<Level_Setting>> LSCtr;
typedef LSCtr::iterator LSIt;
struct Level_Setting{
	string Name;
	Element::Type GoalType[5];
	int GoalNumber[5];

	Element::Type GivenType[5];
	int GivenNumber[5];

	int BestStep;
	bool IsFinished;

};

class Menu_Level;
typedef shared_ptr<Menu_Level> MenuLevelPtr;
class Menu_Level : public Sprite_GL{
private:
	bool m_toogle_show;
	LSCtr m_lss;
	SpritePtr* m_level_btns;
	GL_Vec2* m_btn_pos;
protected:
	void LoadLevelSettings(const char*);
public:
	enum Event{OnHideFinished = 1,OnShowFinished};
	Menu_Level();
	~Menu_Level()
	{
		delete[] m_level_btns;
		delete[] m_btn_pos;
	}
	void Hide()
	{

		Visible = false;
		HandleEvent = false;
		for(unsigned int i = 0;i< m_lss.size();i++)
		{
			m_level_btns[i]->Position = -100.f;
		}

		m_toogle_show = !m_toogle_show;
	}
	void Show()
	{
		Visible = true;


		for(unsigned int i = 0;i< m_lss.size();i++)
		{
			//m_level_btns[i]->AddModifier(new ModifierWait((i+1)*100));
			m_level_btns[i]->Position = -100;
			m_level_btns[i]->AddModifier(new ModifierMove(m_btn_pos[i],800.f+i*20.f));

		}
		m_toogle_show = !m_toogle_show;
		this->AddModifier(new ModifierWait(1000,OnShowFinished,this->shared_from_this()));
	}
	void ReceiveMessage(int msg,void* user_object)
	{
		switch(msg){
case OnHideFinished:

	break;
case OnShowFinished:
	HandleEvent = true;
	break;

		}
	}
	void ToggleMenu()
	{
		if(m_toogle_show)	Hide();
		else	Show();


	}
};