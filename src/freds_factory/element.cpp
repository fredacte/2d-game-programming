#include "panel.h"
#include "element.h"

Element::Type Element::s_gen_list[5];
Element* Element::s_swap_pair = NULL;
int Element::s_generate_time = 5000;

void Element::MoveTo(int element_store){
	Panel::Element_Storage es = (Panel::Element_Storage)element_store;
	Panel* p = Panel::GetInstance();
	if(p->NotFull(es))		//check ok
	{
		ElementCtrPtr ctr;
		ctr =  p->GetEmptyContainer(es);
	
		this->AddModifier(new ModifierMove(ctr->Position,1000.f));
		ctr->element = this;
	}
}


void Element::SetState(State s)
{
	//pre - modify
	if(m_state == s)	return;	//prevent set again
	if(m_state != Rule && m_state != Prepare)
	{	
		TintColor.alpha = m_old_rule_alpha;
		HandleEventMouseMove = false;

	}


	m_state = s;
	//post - modify


	if(m_state == Gen || m_state == Prepare || m_state == Goal){	m_spr_char->Visible = true;}
	else{m_spr_char->Visible = false;}


	switch(m_state){
		case Prepare:
			ZOrder = 0.6f;
			MoveTo(Panel::Lower);
			HandleEventMouseMove = true;
		break;
		case Gen:
			MoveTo(Panel::YinYang);
		break;
		case Goal:
			MoveTo(Panel::Upper);
		break;
		case Rule:
			m_old_rule_alpha = TintColor.alpha ;
			TintColor.alpha = 0.5;
			HandleEventMouseMove =  true;
		break;

	}


}

void Element::OnUpdate(int elapsed)
{

	switch(m_state){
		case Gen:
			{
				Rotation+=( 1 - ((float)m_gen_timer / (float)s_generate_time)) * 10;		//rotate
				if(Rotation > 360)	Rotation = 0;

				m_gen_timer+= elapsed;
				if(m_gen_timer > s_generate_time){
					this->Generate();
					m_gen_timer = 0;
				}
			}
			break;
	


	}

}

void Element::Generate(){
	for(int i = 0;i<5;i++)
	{
		if(s_gen_list[i] == m_type)
		{
			if(++i == 5)	i = 0;

			SetType(s_gen_list[i]);	
			SetNumber(GetNumber()+1);

			this->AddModifier(new ModifierAlpha(1.0f,0.0f,250,ModifierAlpha::Scale));
			m_spr_char->AddModifier(new ModifierAlpha(1.0f,0.0f,250,ModifierAlpha::Scale));
			this->AddModifier(new ModifierAlpha(0.0f,1.0f,250,ModifierAlpha::Scale));
			m_spr_char->AddModifier(new ModifierAlpha(0.0f,1.0f,250,ModifierAlpha::Scale));
			return; 
		}
	}
}

bool Element::Handler::OnMouseDown(SDL_Event* evt,DisplayObject* obj)
{
	Element* element = (Element*)obj;
	//clean up swap rule when next operation is not rule elements
	if(element->m_state != Rule)
	{
		s_swap_pair = NULL;
	}
	Panel* p = Panel::GetInstance();

	switch(element->m_state){
			case Rule:
				{
					//swap the rule by using move modifier
					if(s_swap_pair != NULL && s_swap_pair != element){
						//Swap here
						element->AddModifier(new ModifierMove(s_swap_pair->GetAbsolute(),500.f));
						s_swap_pair->AddModifier(new ModifierMove(element->GetAbsolute(),500.f));
						element->SwapRules(element->GetType(),s_swap_pair->GetType());
						s_swap_pair = NULL;

					}else{
						s_swap_pair = element;
					}
				}
				break;
			case Prepare:
				p->CleanElement(Panel::Lower,element);
				element->SetState(Gen);
				break;
			case Gen:
				{
					p->CleanElement(Panel::YinYang,element);

					//check if match goal
					ElementCtrPtr ctr = p->MatchGoal(element);
					if(ctr == NULL){
						element->SetState(Prepare);	//move to prepare state when not match
					}else{
						//when match, move to target ui and remove them(goal & itself)
						ctr->element->AddModifier(new ModifierAlpha(1,0,1000,ModifierAlpha::Alpha,true));
						ctr->element->AddModifier(new ModifierRemove(0));
						ctr->element = NULL;

						element->AddModifier(new ModifierMove(ctr->Position,1000.f));

						element->AddModifier(new ModifierAlpha(1,0,1000,ModifierAlpha::Alpha,true));

						//send msg to panel, and let panel to handle event
						element->AddModifier(new ModifierWait(0,Panel::OnMatchGoal,
							Panel::GetInstance()->shared_from_this(),NULL));
						element->AddModifier(new ModifierRemove(0));
						
					}
				}
				break;

	}
	//this->Generate();
	//Drag();
	return true;
}
bool Element::Handler::OnMouseOver(SDL_Event* evt,DisplayObject* obj,bool isEndTouch)
{
		Element* element = (Element*)obj;
	if(element->m_state == Rule){
		if(isEndTouch)	element->AddModifier(new ModifierAlpha(1.f,0.7f,250));
		else	element->AddModifier(new ModifierAlpha(0.7f,1.f,250));
	}else if(element->m_state == Prepare && !isEndTouch){
		element->AddModifier(new ModifierAlpha(1.f,1.3f,125,ModifierAlpha::Scale,true));
		element->AddModifier(new ModifierAlpha(1.3f,1.f,125,ModifierAlpha::Scale,true));
	}

	return true;

}