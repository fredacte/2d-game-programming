//	Author:			Cheung Kwok Wai
//	Description:	Game Instance contains Panel & Menu
//	Date:			26/3/2012

#pragma once


#include "..\basic_project\basic_project_header.h"

#include "panel.h"
#include "element.h"
#include "menu.h"
#include "menu_level.h"

class Five_element : public Basic_SDL
{
public:
	enum GameState{Game,MainMenu,LevelMenu};
private:
	SpritePtr		m_bk;
	PanelPtr		m_panel;	//main game
	MenuPtr			m_menu;		//menu
	MenuLevelPtr	m_menu_level;
	GameState		m_gs;
public:
	Five_element();
	
	void StartGame();
	void StartGame(Level_Setting*);
	void StartLevelMenu();
	void ShowMenu();

    bool OnInit();
	void OnEvent(SDL_Event* evt);
};