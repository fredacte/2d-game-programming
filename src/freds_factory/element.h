#pragma once
#include "..\sprite\sprite_gl.h"




class Element : public Sprite_GL{

public:
	enum Type{Metal = 0,Wood ,Water,Fire,Earth,Empty};
	//store rules of generation
	static Type s_gen_list[5];
	enum State{Prepare = 0,Gen,Goal,Rule,NotDecide};

	//handle swap rules
	static Element* s_swap_pair;

	//generate
	static int s_generate_time;		//in millisec
	int m_gen_timer;

	
private:
	class Handler : public EventListener
	{
		bool OnMouseOver(SDL_Event* evt,DisplayObject* obj,bool isEndTouch);
		bool OnMouseDown(SDL_Event* evt,DisplayObject* obj);
	};
	friend class Handler;

	Type m_type;
	State m_state;

	//for rule animation
	bool m_toggleAlpha;
	float m_old_rule_alpha;

	//goal
	int m_number;
	SpritePtr m_spr_char;
	
public:
	
	void SetType(Type type)
	{
		m_type = type;
		SetCurrentFrame(m_type);
	}
	Type GetType(){return m_type;}
	
	void SetNumber(int i)
	{
		m_number = i % 5;
		m_spr_char->SetCurrentFrame(m_number);
	}
	int GetNumber(){return m_number;}
	State GetState(){return m_state;}

	Element(Type t,State s = NotDecide) : Sprite_GL("five_elements.png")
	{	
		SplitFrames(5,1);
		ZOrder = 0.5f;
		SetCenter();
		

		HandleEventMouse = true;
		m_toggleAlpha = true;
		m_gen_timer = 0;
		m_number = 0;

		m_old_rule_alpha = TintColor.alpha;

		Sprite_GL* text = new Sprite_GL("Char.png");
		
		text->SplitFrames(5,1);

		text->ZOrder = ZOrder + 0.2f;

		text->SetCenter();
		m_spr_char = Add(text);


		SetEventListener(new Handler());
		SetType(t);
		SetState(s);
		

		
		

	}
	static void SwapRules(Type a,Type b)
	{
		int ia,ib;
		for(int i = 0;i<5;i++)
		{
			if(s_gen_list[i] == a)	ia = i;
			if(s_gen_list[i] == b)	ib = i;
		}

		Type t = s_gen_list[ia];
		s_gen_list[ia] = s_gen_list[ib];
		s_gen_list[ib] = t;

	}
	void MoveTo(int element_store);
	
	void Generate();
	void OnUpdate(int elapsed);

	// declear in cpp
	void SetState(State s);
	bool OnMouseDown(SDL_Event* evt);
	bool OnMouseUp(SDL_Event* evt);

	bool OnMouseOver(SDL_Event* evt,bool isEndTouch);
};

class Element_Static_Init{
public:
	Element_Static_Init(){
		Element::s_gen_list[0] = Element::Water;
		Element::s_gen_list[1] = Element::Wood;
		Element::s_gen_list[2] = Element::Fire;
		Element::s_gen_list[3] = Element::Earth;
		Element::s_gen_list[4] = Element::Metal;

		Element::s_swap_pair = NULL;

	}
};