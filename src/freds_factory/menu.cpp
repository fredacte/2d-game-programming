#include "menu.h"
#include "..\basic_project\basic_project_main.h"
#include "panel.h"
#include "five_element.h"

Sprite_GL* Menu::CreateGameModeButton(int i)
{
	Sprite_GL* btn = new Sprite_GL("ui_game_mode.png");

	btn->Visible = false;
	btn->SetEventListener(new ButtonHandler());
	btn->SplitFrames(1,2);
	btn->SetCurrentFrame(i);
	btn->SetCenter();
	btn->ZOrder = 0.6f;
	btn->Position = m_btn_loc[2] + 
		GL_Vec2(0,(i == 0 ?  -btn->Center.y : btn->Center.y));

	btn->SetEventMouse(false,false,false);
	return btn;
}
Sprite_GL* Menu::CreateMenuButton(int idx)
{
	int sw = Basic_SDL::GetInstance()->GetScreenWidth();
	int sh = Basic_SDL::GetInstance()->GetScreenHeight(); 

	Sprite_GL* btn = new Sprite_GL("ui_menu.png");
	btn->SplitFrames(2,2);
	btn->SetCenter();
	btn->SetCurrentFrame(idx);

	int sb = btn->GetWidth();
	int bd = 50;

	btn->Position = GL_Vec2((float)(idx%2*225.f + sw / 3),(float)(idx/2*225.f + sh / 4+50));
	btn->ZOrder = 0.5f;

	m_btn_loc[idx] = btn->Position;

	GL_Vec2 hid_loc;
	float offset = 500;
	switch(idx){
		case 0:	hid_loc = GL_Vec2(0,-offset);	break;
		case 1:	hid_loc = GL_Vec2(offset,0);	break;
		case 2:	hid_loc = GL_Vec2(-offset,0);	break;
		case 3:	hid_loc = GL_Vec2(0,offset);	break;
		
	}

	m_btn_hidden_loc[idx] = m_btn_loc[idx] + hid_loc;

	btn->Position = GL_Vec2(m_btn_hidden_loc[idx]);
	btn->SetEventListener(new ButtonHandler());
	btn->SetEventMouse(true,false,true);

	return btn;
}

bool Menu::ButtonHandler::OnMouseDown(SDL_Event* evt,DisplayObject* obj)
{
	//Console::GetInstance()->Trace(str(format("x:[%1%] y:[%2%]")%evt->button.x%evt->button.y));
	Basic_SDL* sdl = Basic_SDL::GetInstance();
	Menu* menu = (Menu*)obj->GetFather().get();

	int idx = -1;
	for(int i = 0;i<4;i++){		//four main button
		if(menu->m_btn[i].get() == obj)		
		{
			switch(i)
			{
			case Start:
				((Five_element*)Basic_SDL::GetInstance())->StartGame();
				menu->ToggleMenu();
				break;

			case YinYang:
				((Five_element*)Basic_SDL::GetInstance())->StartLevelMenu();
				menu->ToggleMenu();
				break;

			case Info:
				{
				obj->Visible = false;
				obj->HandleEventMouse = false;
				for(int i = 0;i<2;i++)
				{
					menu->m_btn_game_mode[i]->Visible = true;
					menu->m_btn_game_mode[i]->HandleEventMouse = true;
				}
				}
				break;

			case Quit:
				sdl->Quit();

				break;
			}
			return true;
		}

	
	}

	for(int i = 0;i<2;i++)		//game mode button
	{
		if(menu->m_btn_game_mode[i].get() == obj)
		{
			switch(i)
			{
				case RealTime:
					Panel::Is_RealTime = true;
				break;
				case TurnBase:
					Panel::Is_RealTime = false;
				break;

			}
			for(int i = 0;i<2;i++)	{
				menu->m_btn_game_mode[i]->Visible = false;
				menu->m_btn_game_mode[i]->HandleEventMouse = false;
			}
			
			menu->m_btn[Info]->Visible = true;
			menu->m_btn[Info]->HandleEventMouse = true;
			return true;
		}
		
	}
	

	return false;
}