
#pragma once
#include "..\sprite\sprite_gl.h"
#include<time.h> 

#include "element.h"
#include "menu_level.h"
#include "..\basic_project\console.h"

class Element_Container : public Sprite_GL{
public:
	Element* element;
	Element_Container(std::string s) : Sprite_GL(s){element = NULL;}
};

class Base : public Sprite_GL{

public:
	SpritePtr m_arrows[5];

	Base() : Sprite_GL("base.png")
	{	
		//this->SetCenter();
		Center.x = 166;
		Center.y = 177;

		ZOrder = 0.14f;

		for(int i = 0;i<5;i++)
		{
			
			Sprite_GL* spr = new Sprite_GL("arrow.png");

			spr->Rotation = (float)i * 360.f / 5.f - 180.f ;
			spr->SetCenter();
			spr->ZOrder = 0.15f;

			m_arrows[i] = Add(spr);
			
		}

		m_arrows[0]->Position = GL_Vec2(0.f,145.f);
		m_arrows[1]->Position = GL_Vec2(-128.35f,49.45f);
		m_arrows[2]->Position = GL_Vec2(-86.75f,-106.3f);
		m_arrows[3]->Position = GL_Vec2(86.75f,-106.3f);
		m_arrows[4]->Position = GL_Vec2(128.35f,49.45f);

		HandleEventMouse = true;
		HandleEventMouseMove = true;

	}
	
	bool OnMouseDown(SDL_Event* evt)
	{

		return true;
	}
	void OnUpdate(int elapsed)
	{


	}
	bool OnMouseUp(SDL_Event* evt)
	{

		return true;
	}
	bool OnMouseOver(SDL_Event* evt,bool isEndTouch){
		for(int i = 0;i<5;i++){
			Sprite_GL* spr = dynamic_cast<Sprite_GL*>(m_arrows[i].get());
			if(isEndTouch)	
			{
				spr->AddModifier(new ModifierWait(i*100));
				spr->AddModifier(new ModifierAlpha(1.f,0.0f,250));
			}
			else	
			{
				spr->AddModifier(new ModifierWait(i*100));
				spr->AddModifier(new ModifierAlpha(0.f,1.0f,250));
			}
		}
		return true;

	}

};

class YinYang : public Element_Container
{
private:
	class Handler : public EventListener{
		bool OnMouseMove(SDL_Event* evt,DisplayObject* obj)
		{
			GL_Vec2 mousePos(evt->button.x,evt->button.y);
			((YinYang*)obj)->Speed =  (mousePos - obj->GetAbsolute()).GetMagnitude() / 5.f;
			return true;
		}
	};
	friend class Handler;
	//rotation speed
	float m_speed;

	//animation
	bool m_toggleAlpha;

public:
	//rotation speed
	float Speed;

	YinYang() : Element_Container("yin_yang.png")
	{	
		Speed = 10;
		SetCenter();

		m_toggleAlpha = true;

		SetEventMouse(true,true,false);

		ZOrder = 0.16f;
		SetEventListener(new Handler());

	}
	void OnUpdate(int elapsed)
	{

		Rotation += (float)elapsed / 1000.0f * Speed;
		if(Rotation > 360)
			Rotation -= 360;



	}
};
class Panel;
typedef shared_ptr<Base> BasePtr;
typedef shared_ptr<Element_Container> ElementCtrPtr;
typedef shared_ptr<Element> ElementPtr;

typedef shared_ptr<Panel> PanelPtr;

class Panel : public Sprite_GL
{

private:
	// sprite
	BasePtr m_base;
	ElementCtrPtr m_sm_yinyang[5];

	ElementCtrPtr m_cellUpper[5];
	ElementCtrPtr m_cellLower[5];
	ElementPtr m_elements_gRule[5];
	ElementCtrPtr* m_element_containers[3];


	// Level Setting
	Level_Setting * m_ls;
	int m_last_empty_conatiner_index;
	GL_Vec2 m_element_rules_pos[5];

	// rule
	Element::Type* m_generation_rule;

	// singleton
	static Panel* instance;

	 

	//toggle
	bool m_toggleHidden;
protected:
	ElementCtrPtr CreateYinYang(float x,float y)
	{
		ElementCtrPtr yy = ElementCtrPtr(new ::YinYang());
		yy->Position = GL_Vec2(x,y);




		return yy;
	}
	ElementCtrPtr CreateCell(float x,float y)
	{
		ElementCtrPtr s = ElementCtrPtr(new Element_Container("Cell.png"));
		s->ZOrder = 0.15f;
		s->SetCenter();
		s->Position = GL_Vec2(x,y) + s->Center;
		return s;
	}
	void InitPanelElement()
	{
		Element_Static_Init();
		srand(static_cast<unsigned int>(time(0)));
		
		m_generation_rule = Element::s_gen_list;




		float dx = 50,dy = -25;

		//small yin yang for generation
		m_sm_yinyang[0] = CreateYinYang(243.05f + dx,266.40f + dy);
		m_sm_yinyang[1] = CreateYinYang(329.3f + dx,266.4f + dy);
		m_sm_yinyang[2] = CreateYinYang(357.25f + dx,348.70f +dy);
		m_sm_yinyang[3] = CreateYinYang(286.6f + dx,405.2f+dy);
		m_sm_yinyang[4] = CreateYinYang(220.1f + dx,348.7f+dy);

		//position of rules element
		m_element_rules_pos[0] = GL_Vec2(287.95f+dx,182.10f+dy);
		m_element_rules_pos[1] = GL_Vec2(420.35f+dx,282.05f+dy);
		m_element_rules_pos[2] = GL_Vec2(368.95f+dx,444.90f+dy);
		m_element_rules_pos[3] = GL_Vec2(205.35f+dx,444.90f+dy);
		m_element_rules_pos[4] = GL_Vec2(154.65f+dx,283.05f+dy);


		m_base = BasePtr(new Base());
		m_base->Position = GL_Vec2(285.f+dx,319.9f+dy);
		Add(m_base);

		float dx2 = 25,dy2 = -0;

		for(int i = 0;i < 5;i++)
		{


			m_cellUpper[i] = CreateCell(dx2,25.f+i*108.f+dy2);
			m_cellLower[i] = CreateCell(539.f+dx2,25.f+i*108.f+dy2);
			Add(m_cellUpper[i]);
			Add(m_cellLower[i]);
			Add(m_sm_yinyang[i]);

			m_elements_gRule[i] = ElementPtr(new Element(m_generation_rule[i],Element::Rule));

			Add(m_elements_gRule[i]);
			m_elements_gRule[i]->SetAbsPosition(m_element_rules_pos[i].x,m_element_rules_pos[i].y);



		}

		m_element_containers[YinYang]	= m_sm_yinyang;
		m_element_containers[Upper]		= m_cellUpper;
		m_element_containers[Lower]		= m_cellLower;

		
	}
	
public:
	// game mode
	static bool Is_RealTime;

	enum Event_Type{OnMatchGoal = 0};
	
	void Start()
	{
		//should add before m_element_containers init
		if(m_ls == NULL){	//if no level setting, use random
			for(int i = 0;i<5;i++)
			{
				AddLowerRandomElement();
				AddUpperRandomElement();
			}
		}else
		{
			for(int i = 0;i<5;i++)
			{
				AddElement(m_ls->GoalType[i],m_ls->GoalNumber[i],Element::Goal);
				AddElement(m_ls->GivenType[i],m_ls->GivenNumber[i],Element::Prepare);
			}
		}
	}
	Panel() : Sprite_GL()
	{
		instance = this;
		SetEventMouse(true,false,true);
		m_toggleHidden = false;
		m_ls = NULL;
		InitPanelElement();

		

	}
	Panel(Level_Setting *ls) : Sprite_GL()
	{
		instance = this;
		SetEventMouse(true,false,true);
		m_toggleHidden = false;
		m_ls = ls;
		InitPanelElement();
	}
	void ReceiveMessage(int msg,void* user_object)
	{
		switch(msg){
			case OnMatchGoal:
				if(m_ls == NULL){
					AddLowerRandomElement();
					AddUpperRandomElement();
				}
			break;
		
		}

	}
	bool OnMouseDown(SDL_Event *evt)
	{
		

		
		return true;
	}
	void TogglePanel()
	{
		if(m_toggleHidden)	Show();
		else	Hide();
		
	}
	void Hide()
	{
		HandleEventChild = false;
		UpdateChild = false;
		AddModifier(new ModifierAlpha(TintColor.alpha,0,100,ModifierAlpha::Alpha,true));
		m_toggleHidden = !m_toggleHidden;
	}
	void Show()
	{
		HandleEventChild = true;
		UpdateChild = true;
		AddModifier(new ModifierAlpha(TintColor.alpha,1,100,ModifierAlpha::Alpha,true));
		m_toggleHidden = !m_toggleHidden;
	}
	
	enum Element_Storage{YinYang = 0,Upper,Lower};
	static Panel* GetInstance(){return instance;}
	ElementCtrPtr GetElementContainer(Element_Storage es,int index){return m_element_containers[es][index % 5];}
	ElementCtrPtr GetElementContainer(Element_Storage es,Element* who_hold_this)
	{
		for(int i = 0;i<5;i++)
		{
			if(m_element_containers[es][i]->element == who_hold_this)	return m_element_containers[es][i];
		}
		return ElementCtrPtr();
	}
	bool NotFull(Element_Storage es){
		bool b = false;
		for(int i =0;i<5;i++)	b |= !m_element_containers[es][i]->element;
		return b;
	}

	ElementCtrPtr GetEmptyContainer(Element_Storage es)
	{
		for(int i =0;i<5;i++)
			if(m_element_containers[es][i]->element == NULL){
				return m_element_containers[es][i];
			}

			return ElementCtrPtr();
	}
	void CleanElement(Element_Storage es,Element* e)
	{
		ElementCtrPtr yy = GetElementContainer(es,e);
		yy->element = NULL;
	}
	ElementCtrPtr MatchGoal(Element* e)
	{
		for(int i = 0;i<5;i++)
		{
			if(m_cellUpper[i]->element != NULL && m_cellUpper[i]->element->GetNumber() == e->GetNumber() &&
				m_cellUpper[i]->element->GetType() == e->GetType())
			{
				return m_cellUpper[i];
			}
		}
		return ElementCtrPtr();
	}
	
	void AddElement(Element::Type t,int number,Element::State s)
	{
		ElementPtr element = ElementPtr(new Element(t,s));
		element->SetNumber(number);
		Add(element);
		element->SetAbsPosition(m_base->GetAbsoluteX(),m_base->GetAbsoluteY());
		
		
	}
	void AddLowerRandomElement()
	{
		AddElement((Element::Type)(rand() % 5),0,Element::Prepare);
		
	}
	void AddUpperRandomElement()
	{
		AddElement((Element::Type)(rand() % 5),rand() % 5,Element::Goal);
	}

	
	
	~Panel()
	{	
		//becareful not set static instance to null here
		//because panel will not be deleted instantly to prevent update crach.
		//so two panel can exist in main sprite in only one update
	}

};

