
#pragma once
#include "..\sprite\sprite_text.h"

class Menu;
typedef shared_ptr<Menu> MenuPtr;


class Menu : public Sprite_GL{

private:
	//four main button
	SpritePtr m_btn[4];
	GL_Vec2 m_btn_loc[4];
	GL_Vec2 m_btn_hidden_loc[4];

	//game mode button
	SpritePtr m_btn_game_mode[2];
	
	


	bool m_toogle_show;

	//button handler
	class ButtonHandler : public EventListener
	{
		public:
		bool OnMouseDown(SDL_Event* evt,DisplayObject* obj);
	};
	friend class ButtonHandler;

protected:
	Sprite_GL* CreateMenuButton(int );	//0-4
	Sprite_GL* CreateGameModeButton(int );	//0-2

public:
	enum ButtonMain{Start = 0,YinYang,Info,Quit};
	enum ButtonGameMode{RealTime = 0,TurnBase};

	Menu() : Sprite_GL()
	{	
		for(int i = 0;i<4;i++)
			m_btn[i] = Add(CreateMenuButton(i));

		for(int i = 0;i<2;i++)
			m_btn_game_mode[i] = Add(CreateGameModeButton(i));
	
		HandleEventMouseLock = true;
		m_toogle_show = false;
	}
	void Show()
	{
		//m_text->SetText("Show");
		for(int i = 0;i<4;i++){
			Modifier** mg = new Modifier*[2];
			mg[0] = new ModifierAlpha(m_btn[i]->TintColor.alpha,1.0f,400);
			mg[1] = new ModifierMove(m_btn_loc[i],2000);
			m_btn[i]->AddModifier(new ModifierGroup(mg,2));
		}
		m_toogle_show = !m_toogle_show;
	}
	void Hide()
	{
		//m_text->SetText("Hide");
		for(int i = 0;i<4;i++){
			Modifier** mg = new Modifier*[2];
			mg[0] = new ModifierMove(m_btn_hidden_loc[i],2000);
			mg[1] =	new ModifierAlpha(m_btn[i]->TintColor.alpha,0,200);
			
			m_btn[i]->AddModifier(new ModifierGroup(mg,2));
		}
		m_toogle_show = !m_toogle_show;
	}
	void ToggleMenu()
	{
		if(m_toogle_show)	Hide();
		else	Show();
		

	}
	

	
};