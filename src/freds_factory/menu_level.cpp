#include "menu_level.h"
#include "five_element.h"
#include <boost/lexical_cast.hpp>
#include "five_element.h"

bool Menu_Button::Handler::OnMouseMove(SDL_Event *evt,DisplayObject* obj)
{
	
	GL_Vec2 mousePos(evt->button.x,evt->button.y);


	Speed =  (mousePos - obj->GetAbsolute()).GetMagnitude() / 2.f;

	return true;
}
bool Menu_Button::Handler::OnMouseOver(SDL_Event* evt,DisplayObject* obj,bool isEndTouch)
{
	Level_Setting* ls = (Level_Setting*)obj->User_Object;
	Menu_Button* btn = (Menu_Button*)obj;
	

	if(isEndTouch){
		Name->Visible = true;
		Name->AddModifier(new ModifierAlpha(Name->TintColor.alpha,0,250));
		btn->AddModifier(new ModifierAlpha(btn->TintColor.alpha,1,250));
		btn->AddModifier(new ModifierWait(0,OnHideTextFinished));
	}else{
		if(Name.get() == NULL)
		{
			Sprite_Text* spr = new Sprite_Text(ls->Name,GL_Color(0,0,0,0),"Hobo.ttf",40);
			spr->ZOrder =0.35f;
			spr->SetCenter();
			spr->TintColor.alpha = 0.f;
			Name = btn->Add(spr);
		}
		Name->Visible = true;
		Name->AddModifier(new ModifierAlpha(Name->TintColor.alpha,1,250));
		btn->AddModifier(new ModifierAlpha(btn->TintColor.alpha,0,250));
		btn->AddModifier(new ModifierWait(0,OnShowTextFinished));
	}
	return true;

}
bool Menu_Button::Handler::OnMouseDown(SDL_Event* evt,DisplayObject* obj)
{
	Menu_Button* btn = (Menu_Button*)obj;
	if(btn->HitTest(GL_Vec2(evt->button.x,evt->button.y))){
		((Menu_Level*)btn->GetFather().get())->Hide();
		((Five_element*)Basic_SDL::GetInstance())->StartGame((Level_Setting*)obj->User_Object);
		return true;
	}
	return false;

}

Menu_Level::Menu_Level() : Sprite_GL()
{

	m_toogle_show = true;

	LoadLevelSettings("five_element_levels.xml");

	m_level_btns = new SpritePtr[m_lss.size()];	int count = 0;
	m_btn_pos = new GL_Vec2[m_lss.size()];
	for(LSIt it = m_lss.begin(), end = m_lss.end();it != end;++it)
	{
		m_btn_pos[count] = GL_Vec2((float)(count%4) * 135.f + 130.f,(float)(count/4)*110.f + 125.f);

		Menu_Button *btn = new Menu_Button();
		btn->User_Object = (void*)(*it).get();
		btn->Position = GL_Vec2(-100,-100);

		btn->ZOrder = 0.3f;
		m_level_btns[count] = SpritePtr(btn);
		Add(m_level_btns[count]);


		count++;
	}
	//SetHandleMouse(true,false,true);


}



void Menu_Level::LoadLevelSettings(const char* filename)
{
	TiXmlDocument doc(filename);
	bool loadOkay = doc.LoadFile();

	if(loadOkay){

		TiXmlNode* levels =  doc.FirstChild("Levels");
		//iterate all the level in levels Node
		for ( TiXmlNode* level = levels->FirstChild(); level != 0; level = level->NextSibling())
		{
			Level_Setting* ls = new Level_Setting();	int count = 0;
			ls->Name = level->Value();

			TiXmlAttribute* lv_atb = level->ToElement()->FirstAttribute();
			//get its attribute
			while(lv_atb){

				string val_str = lv_atb->Value();
				switch(count)
				{
				case 0://Goal	
					{

						for(unsigned int i = 0;i<val_str.size() / 2;i++)
						{
							ls->GoalType[i] = (Element::Type)(lexical_cast<int>(val_str[i]));
							ls->GoalNumber[i] = lexical_cast<int>(val_str[i+5]);
						}
					}
					break;
				case 1://Given
					for(unsigned int i = 0;i<val_str.size() / 2;i++)
					{
						ls->GivenType[i] = (Element::Type)(lexical_cast<int>(val_str[i]));
						ls->GivenNumber[i] = lexical_cast<int>(val_str[i+5]);
					}

					break;
				case 2://finished
					ls->IsFinished = lexical_cast<bool>(val_str);
					break;
				case 3://best step
					ls->BestStep = lexical_cast<int>(val_str);
					break;
				}	


				lv_atb = lv_atb->Next();
				count++;
			}
			m_lss.push_back(shared_ptr<Level_Setting>(ls));
		}

	}
}