//
//  This example is based on Focus on SDL sample code 
//
//  Filename: lab4a.cpp
//
//	Program Description:
//	
//		This C++/SDL code draws some random rectangle on the screen.
//

// These two lines link in the required SDL components for our project. 
// Alternatively, we could have linked them in our project settings.    
#pragma comment(lib, "sdl.lib")
#pragma comment(lib, "sdlmain.lib")

#include "sdl.h"

//include ability to exit program
#include <stdlib.h>

//screen dimensions
const int SCREEN_WIDTH=640;
const int SCREEN_HEIGHT=480;

//display surface
SDL_Surface* g_pDisplaySurface = NULL;

//event structure
SDL_Event g_Event;

//rectangle
SDL_Rect g_Rect;

//color components
Uint8 g_Red, g_Green, g_Blue;

//color value
Uint32 g_Color;

//main function
int main(int argc, char* argv[])
{
	//initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO)==-1)
	{
		//error initializing SDL

		//report the error
		fprintf(stderr,"Could not initialize SDL!\n");

		//end the program
		exit(1);
	}
	else
	{
		//SDL initialized

		//report success
		fprintf(stdout,"SDL initialized properly!\n");

		//set up to uninitialize SDL at exit
		atexit(SDL_Quit);
	}

	//create windowed environment
	g_pDisplaySurface = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,0,SDL_ANYFORMAT);

	//error check
	if (g_pDisplaySurface == NULL)
	{
		//report error
		fprintf(stderr,"Could not set up display surface!\n");

		//exit the program
		exit(1);
	}

	//repeat forever
	for(;;)
	{
		//wait for an event
		if(SDL_PollEvent(&g_Event)==0)
		{
			//no event, so draw rectangle

			//create a random color
			g_Red=rand()%256;
			g_Green=rand()%256;
			g_Blue=rand()%256;
			g_Color=SDL_MapRGB(g_pDisplaySurface->format,g_Red,g_Green,g_Blue);

			//create a random rectangle
			g_Rect.x=rand()%SCREEN_WIDTH;
			g_Rect.y=rand()%SCREEN_HEIGHT;
			g_Rect.w=rand()%(SCREEN_WIDTH-g_Rect.x);
			g_Rect.h=rand()%(SCREEN_HEIGHT-g_Rect.y);

			//fill the rectangle
			SDL_FillRect(g_pDisplaySurface,&g_Rect,g_Color);

			//update the screen
			SDL_UpdateRect(g_pDisplaySurface,0,0,0,0);
		}
		else
		{
			//event occurred, check for quit
			if(g_Event.type==SDL_QUIT) break;
		}
	}

	//normal termination
	fprintf(stdout,"Terminating normally.\n");


	//return 0
	return(0);
}