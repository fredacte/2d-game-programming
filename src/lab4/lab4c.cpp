//
//  This example is based on Focus on SDL sample code 
//
//  Filename: lab4c.cpp
//
//	Program Description:
//	
//		This C++/SDL code blit a bitmap randomly on the screen.
//

// These two lines link in the required SDL components for our project. 
// Alternatively, we could have linked them in our project settings.    
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")

//include SDL stuff
#include "SDL.h"

//include ability to exit program
#include <stdlib.h>

//screen dimensions
const int SCREEN_WIDTH=640;
const int SCREEN_HEIGHT=480;

//display surface
SDL_Surface* g_pDisplaySurface = NULL;

//bitmap surface
SDL_Surface* g_pBitmapSurface = NULL;

//event structure
SDL_Event g_Event;

//source and destination rectangles
SDL_Rect g_SrcRect,g_DstRect;

//main function
int main(int argc, char* argv[])
{
	//initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO)==-1)
	{
		//error initializing SDL

		//report the error
		fprintf(stderr,"Could not initialize SDL!\n");

		//end the program
		exit(1);
	}
	else
	{
		//SDL initialized

		//report success
		fprintf(stdout,"SDL initialized properly!\n");

		//set up to uninitialize SDL at exit
		atexit(SDL_Quit);
	}

	//create windowed environment
	g_pDisplaySurface = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,0,SDL_ANYFORMAT);

	//error check
	if (g_pDisplaySurface == NULL)
	{
		//report error
		fprintf(stderr,"Could not set up display surface!\n");

		//exit the program
		exit(1);
	}

	//load in the bitmap
	g_pBitmapSurface=SDL_LoadBMP("ball.bmp");

	//error check
	if(g_pBitmapSurface==NULL)
	{
		//report error
		fprintf(stderr,"Could not load bitmap surface!\n");

		//exit the program
		exit(1);
	}

	//set the widths of the source and destination rectangles
	g_SrcRect.w=g_DstRect.w=g_pBitmapSurface->w;
	g_SrcRect.h=g_DstRect.h=g_pBitmapSurface->h;

	//set source rect x and y to 0
	g_SrcRect.x=g_SrcRect.y=0;

	//repeat forever
	for(;;)
	{
		//wait for an event
		if(SDL_PollEvent(&g_Event)==0)
		{
			//no event, so blit image onto display

			//pick a random destination location
			g_DstRect.x=rand()%(SCREEN_WIDTH-g_DstRect.w);
			g_DstRect.y=rand()%(SCREEN_HEIGHT-g_DstRect.h);

			//blit
			SDL_BlitSurface(g_pBitmapSurface,&g_SrcRect,g_pDisplaySurface,&g_DstRect);

			//update the screen
			SDL_UpdateRect(g_pDisplaySurface,0,0,0,0);
		}
		else
		{
			//event occurred, check for quit
			if(g_Event.type==SDL_QUIT) break;
		}
	}

	//normal termination
	fprintf(stdout,"Terminating normally.\n");

	//return 0
	return(0);
}