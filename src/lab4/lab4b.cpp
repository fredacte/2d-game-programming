//
//  This example is based on Focus on SDL sample code 
//
//  Filename: lab4b.cpp
//
//	Program Description:
//	
//		This C++/SDL code draws some random color pixel on the screen.
//

// These two lines link in the required SDL components for our project.
// Alternatively, we could have linked them in our project settings.   
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")

//include SDL stuff
#include "SDL.h"

//include ability to exit program
#include <stdlib.h>

//include memcpy
#include <memory.h>

//screen dimensions
const int SCREEN_WIDTH=640;
const int SCREEN_HEIGHT=480;

//display surface
SDL_Surface* g_pDisplaySurface = NULL;

//event structure
SDL_Event g_Event;

//color components
Uint8 g_Red, g_Green, g_Blue;

//color value
Uint32 g_Color;

//x and y locations
Uint16 g_x,g_y;

//temporary data pointer
char* g_pData;

//main function
int main(int argc, char* argv[])
{
	//initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO)==-1)
	{
		//error initializing SDL

		//report the error
		fprintf(stderr,"Could not initialize SDL!\n");

		//end the program
		exit(1);
	}
	else
	{
		//SDL initialized

		//report success
		fprintf(stdout,"SDL initialized properly!\n");

		//set up to uninitialize SDL at exit
		atexit(SDL_Quit);
	}

	//create windowed environment
	g_pDisplaySurface = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,0,SDL_ANYFORMAT);

	//error check
	if (g_pDisplaySurface == NULL)
	{
		//report error
		fprintf(stderr,"Could not set up display surface!\n");

		//exit the program
		exit(1);
	}

	//repeat forever
	for(;;)
	{
		//wait for an event
		if(SDL_PollEvent(&g_Event)==0)
		{
			//no event, so draw pixel

			//create a random color
			g_Red=rand()%256;
			g_Green=rand()%256;
			g_Blue=rand()%256;
			g_Color=SDL_MapRGB(g_pDisplaySurface->format,g_Red,g_Green,g_Blue);

			//determine random location
			g_x=rand()%SCREEN_WIDTH;
			g_y=rand()%SCREEN_HEIGHT;

			//check if surface needs locking
			if(SDL_MUSTLOCK(g_pDisplaySurface))
			{
				//lock the surface
				if(SDL_LockSurface(g_pDisplaySurface)==-1)
				{
					//error
					fprintf(stderr,"Could not lock display surface!\n");

					//exit
					exit(1);
				}
			}

			//grab pixel pointer
			g_pData=(char*)g_pDisplaySurface->pixels;

			//vertical offset
			g_pData+=(g_y*g_pDisplaySurface->pitch);

			//horizontal offset
			g_pData+=(g_x*g_pDisplaySurface->format->BytesPerPixel);

			//copy from color to frame buffer
			memcpy(g_pData,&g_Color,g_pDisplaySurface->format->BytesPerPixel);

			//unlock the surface
			if(SDL_MUSTLOCK(g_pDisplaySurface))
			{
				SDL_UnlockSurface(g_pDisplaySurface);
			}

			//update the screen
			SDL_UpdateRect(g_pDisplaySurface,0,0,0,0);
		}
		else
		{
			//event occurred, check for quit
			if(g_Event.type==SDL_QUIT) break;
		}
	}

	//normal termination
	fprintf(stdout,"Terminating normally.\n");


	//return 0
	return(0);
}