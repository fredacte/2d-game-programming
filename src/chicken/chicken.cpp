//
//	Subject:		SM2603 - 2D Game Programming
//	Assignment:		2
//	Student Name:	
//	Student Number: 
//	Date:	
//
//  Filename: Assignment2.cpp
//
//  Note: This example is based on Bill Kendrick's Chicken Game Example
//
//	Program Description:
//	
//		This C++/SDL code simulate a classic frogger game with a chicken!
//		.... put in your own description here ....
//
//

// These 4 lines link in the required SDL components for our project. 
// Alternatively, we could have linked them in our project settings.    
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")
#pragma comment(lib, "SDL_image.lib")
#pragma comment(lib, "SDL_mixer.lib")

#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )
//
// include SDL header
//
#include <stdio.h>
#include <stdlib.h>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

//
//	Define game constant
//
const int FALSE	= 0;
const int TRUE	= 1;	

const int SCREEN_WIDTH	= 640;			// screen dimension
const int SCREEN_HEIGHT	= 480;

const int CHICKEN_IMAGE_SIZE = 32;		// 32 x 32 pixels
const int CHICKEN_MOVE_DIST = 4;		// the chicken moving at 4 pixel at a time
const int NUM_CHICKEN_ANIM_FRAME = 2;	// the no. of animating frame for chicken

// the initial chicken position
const int INIT_CHICKEN_POS_X = ((SCREEN_WIDTH - CHICKEN_IMAGE_SIZE) / 2);
const int INIT_CHICKEN_POS_Y = (SCREEN_HEIGHT - CHICKEN_IMAGE_SIZE);

const int CAR_IMAGE_WIDTH = 64;		// width of car image
const int CAR_MOVE_DIST = 4;		// the car moving at 4 pixel at a time
const int CAR_SEP_DIST = 80;		// distance between 2 cars
const int NUM_CAR_LANE = 8;			// number or car lanes

const int UP	= 0;				// up key
const int DOWN	= 1;				// down key
const int RIGHT	= 2;				// right key
const int LEFT	= 3;				// left key

const int NUM_CARS	= 48;
const int LANE_DIST = 76;			// distance between 2 lane of traffic

//
//	Structure for the Car Information
//
typedef struct STRU_CAR_INFO {
	int x;			// car position in x
	int y;			// car position in y
	int lane;		// car lane number
	int hide;		// hide the car?
}	STRU_CAR_INFO;

//
//	Define local function prototypes
//
SDL_Surface*	Load_Image(char *szFname);
Mix_Chunk*		Load_Sound(char *szFname);

// 
//	IsCarHide() - an inline function which decide whether 
//				  the new car generated should be hide or not?
//
inline int IsCarHide(void)
{
	return ((rand() % 10) < 3);
}



//
//	main() - main program.
//
int 
main(int argc, char *argv[])
{
	SDL_Surface *pScreen;	// Game primary screen
	
	// Pointers to chicken image
	SDL_Surface *apChicken_Image_Left[2];	// Image Left
	SDL_Surface *apChicken_Image_Right[2];	// Image Right
	SDL_Surface *pChicken_Image_Hurt;		// Image Hurt

	// Pointers to car image 
	SDL_Surface *pCar_Image_Left;			// Image Left
	SDL_Surface *pCar_Image_Right;			// Image Right

	// Pointers to game sounds
	Mix_Chunk *pChickenSound;	// Chicken sound
	Mix_Chunk *pTrafficSound;	// Traffic sound
	Mix_Chunk *pHornSound;		// Horn sound
	Mix_Chunk *pScoreSound;		// Score sound
	
	// Chicken variables
	int iChickenPosX = INIT_CHICKEN_POS_X;	// chicken position in x
	int iChickenPosY = INIT_CHICKEN_POS_Y;	// chicken position in y
	int iChickenFacing = LEFT;				// chicken facing direction
	int iAnimFrame = 0;						// chicken animated frame
	int iChickenHitCount = 0;				// chicken hit counter
	int iScore = 0;							// player score

	// Car variable
	STRU_CAR_INFO cars[NUM_CARS];			// Car information

	// Timer variables
	Uint32 dwStartTime;		// start time
	Uint32 dwEndTime;		// end time

	int bKeyPressed[4];		// record the arrow key pressed
	int	bLoopDone = FALSE;	// true if the quit the game loop
	SDL_Event	event;		// SDL event
	SDL_Rect	dest;		// for drawing use

	//
	// Initialize SDL Video and Audio
	//
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		fprintf(stderr, "Error init'ing SDL: %s\n", SDL_GetError());
		exit(1);
	}

	//
	// Setup the primary display: 640 x 480, double buffer
	//
	pScreen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_DOUBLEBUF);

	if (pScreen == NULL)
	{
		fprintf(stderr, "Error: Can't open window! %s\n", SDL_GetError());
		exit(1);
	}

	//
	// Load in chicken graphics
	//
	apChicken_Image_Left[0]		= Load_Image("chickenL1.png");
	apChicken_Image_Left[1]		= Load_Image("chickenL2.png");
	apChicken_Image_Right[0]	= Load_Image("chickenR1.png");
	apChicken_Image_Right[1]	= Load_Image("chickenR2.png");
	pChicken_Image_Hurt			= Load_Image("chicken_hurt.png");

	//
	// Load in car graphics
	//
	pCar_Image_Left		= Load_Image("carL.png");
	pCar_Image_Right	= Load_Image("carR.png");

	//
	// Initialise the game audio
	//
	if (Mix_OpenAudio(44100, AUDIO_S16, 2, 256) < 0)
	{
		fprintf(stderr, "Error opening audio: %s\n", Mix_GetError());
		exit(1);
	}

	//
	// Load in game music
	//
	pChickenSound = Load_Sound("chicken.wav");
	pTrafficSound = Load_Sound("traffic.wav");
	pHornSound    = Load_Sound("horn.wav");
	pScoreSound   = Load_Sound("score.wav");

	//
	// Initialise the car position
	//
	for (int i = 0; i < NUM_CARS; i++)
	{
		cars[i].lane = (i / NUM_CAR_LANE);
		cars[i].x = (i % NUM_CAR_LANE) * CAR_SEP_DIST;
		cars[i].y = cars[i].lane * LANE_DIST;
		cars[i].hide = IsCarHide();
	}

	//
	// clear the keypress array
	//
	for (int i = 0; i < 4; i++)
		bKeyPressed[i] = FALSE;

	//
	// The Main Game Loop
	//
	do
	{
		// Record the current time
		dwStartTime = SDL_GetTicks();

		//
		// Handle they keyboard events here
		//
		while (SDL_PollEvent(&event) > 0)
		{
			if (event.type == SDL_QUIT)
			{
				// Quit event! (Window close, kill signal, etc.)
				bLoopDone = TRUE;
			}
			// Check for key pressed
			else if (event.type == SDL_KEYDOWN)
			{
				// Check individual key
				SDLKey key = event.key.keysym.sym;

				if (key == SDLK_ESCAPE)
				{
					// Quit on escape key
					bLoopDone = TRUE;
				}
				else if (key == SDLK_UP || key == SDLK_DOWN ||
					key == SDLK_RIGHT || key == SDLK_LEFT)
				{
					// record the key stroke
					bKeyPressed[(int)(key - SDLK_UP)] = TRUE;

					// change the chicken facing base on the left/right key
					if (key == SDLK_LEFT)
						iChickenFacing = LEFT;
					else if (key == SDLK_RIGHT)
						iChickenFacing = RIGHT;
				}
			}
			// Check for key released
			else if (event.type == SDL_KEYUP)
			{
				SDLKey key = event.key.keysym.sym;

				if (key == SDLK_UP || key == SDLK_DOWN ||
					key == SDLK_RIGHT || key == SDLK_LEFT)
				{
					// record the key stroke
					bKeyPressed[(int)(key - SDLK_UP)] = FALSE;
				}
			}
		}

		//
	    // Try move the chicken if it is not being hit
		//
		if (iChickenHitCount == 0)
		{
			int	bAnim = FALSE;	// animate the chicken?

			// User is still in control of the chicken
			if (bKeyPressed[UP])
			{
				iChickenPosY -= CHICKEN_MOVE_DIST;

				// if the chicken off the top of the screen
				if (iChickenPosY < -CHICKEN_IMAGE_SIZE)
				{
					// scoring for successful reach the top of screen
					iScore++;

					// reset the chicken position
					iChickenPosX = INIT_CHICKEN_POS_X;
					iChickenPosY = INIT_CHICKEN_POS_Y;

					Mix_PlayChannel(0, pScoreSound, 0);
				}

				bAnim = TRUE;
			}
			else if (bKeyPressed[DOWN])
			{
				iChickenPosY += CHICKEN_MOVE_DIST;
				bAnim = TRUE;
			}

			if (bKeyPressed[RIGHT])
			{
				iChickenPosX += CHICKEN_MOVE_DIST;

				// avoid leaving the screen
				if (iChickenPosX > (SCREEN_WIDTH - CHICKEN_IMAGE_SIZE))
					iChickenPosX = SCREEN_WIDTH - CHICKEN_IMAGE_SIZE;
				bAnim = TRUE;
			}
			else if (bKeyPressed[LEFT])
			{
				iChickenPosX -= CHICKEN_MOVE_DIST;

				// avoid leaving the screen
				if (iChickenPosX < 0)
					iChickenPosX = 0;
				bAnim = TRUE;
			}

			// Animate the chicken
			if (bAnim)
				iAnimFrame = (iAnimFrame + 1) % NUM_CHICKEN_ANIM_FRAME;
		}
		else // the poor chicken being hit
		{
			if (iChickenHitCount > 7)
			{
				// For the first few moments, move down and shake about
				iChickenPosY += CHICKEN_MOVE_DIST;
				iChickenPosX += ((rand() % 8) - 4);
			}

			// Reduce the hit count
			iChickenHitCount--;
		}

		//
		// Keep the chicken on the screen
		// Done here because chicken could move down manually _or_ when hurt!
		//
		if (iChickenPosY > INIT_CHICKEN_POS_Y)
			iChickenPosY = INIT_CHICKEN_POS_Y;

		//
		// Move the cars around
		//
		for (int i = 0; i < NUM_CARS; i++)
		{
			// the direction of the car depends on which lane they're in
			if (cars[i].lane % 2)
			{
				cars[i].x -= CAR_MOVE_DIST;

				// if the car move to out of screen (left),
				// put it on the right hand side of screen
				if (cars[i].x < -CAR_IMAGE_WIDTH)   
				{
					cars[i].x = SCREEN_WIDTH;     /* (the car image is 64 pixels wide) */
					cars[i].hide = IsCarHide();
				}
			}
			else
			{
				cars[i].x += CAR_MOVE_DIST;

				// if the car move to out of screen (right),
				// put it on the left hand side of screen
				if (cars[i].x > SCREEN_WIDTH)
				{
					cars[i].x = -CAR_IMAGE_WIDTH;
					cars[i].hide = IsCarHide();
				}
			}
		}

		//
		//	Check for collisions
		//
		for (int i = 0; i < NUM_CARS; i++)
		{
			if (cars[i].hide == FALSE)
			{
				if ((cars[i].x + CAR_IMAGE_WIDTH >= iChickenPosX)
					&& (cars[i].x < iChickenPosX + CHICKEN_IMAGE_SIZE)
					&& ((cars[i].y + CHICKEN_IMAGE_SIZE) >= iChickenPosY)
					&& (cars[i].y < (iChickenPosY + CHICKEN_IMAGE_SIZE)))
				{
					iChickenHitCount = 10;

					Mix_PlayChannel(0, pChickenSound, 0);
				}
			}
		}

		//
		// Redraw the whole screen with the gray color
		//
		SDL_FillRect(pScreen, NULL, SDL_MapRGB(pScreen->format, 128, 128, 128));

		//
		// Draw the chicken here
		//
		dest.x = iChickenPosX;
		dest.y = iChickenPosY;

		if (iChickenHitCount > 0)
			SDL_BlitSurface(pChicken_Image_Hurt, NULL, pScreen, &dest);
		else if (iChickenFacing == LEFT)
			SDL_BlitSurface(apChicken_Image_Left[iAnimFrame], NULL, pScreen, &dest);
		else if (iChickenFacing == RIGHT)
			SDL_BlitSurface(apChicken_Image_Right[iAnimFrame], NULL, pScreen, &dest);

		//
		// Draw the cars
		//
		for (int i = 0; i < NUM_CARS; i++)
		{
			if (cars[i].hide == FALSE)
			{
				dest.x = cars[i].x;
				dest.y = cars[i].y;

				/* Which direction it's facing depends on the lane it's in! */
				if (cars[i].lane % 2)
					SDL_BlitSurface(pCar_Image_Left, NULL, pScreen, &dest);
				else
					SDL_BlitSurface(pCar_Image_Right, NULL, pScreen, &dest);
			}
		}

		// Update the display
		SDL_Flip(pScreen);

		//
		// Play traffic sounds and random horn honks
		//
		if (!Mix_Playing(1) && ((rand() % 80) < 1))
		{
			// randomly play the traffic and horn sounds
			if (rand() % 2)
				Mix_PlayChannel(1, pTrafficSound, 0);
			else
				Mix_PlayChannel(1, pHornSound, 0);
		}

		//
		// Pause at the end if necessary to control the frame rate
		//
		dwEndTime = SDL_GetTicks();
		if (dwEndTime < dwStartTime + (1000 / 60))
		{
			SDL_Delay(dwStartTime + (1000 / 60) - dwEndTime);
		}

	} while (!bLoopDone);


	// Close up SDL!
	Mix_CloseAudio();
	SDL_Quit();

	return(0);
}


//
//	Load_Image() - load an image, abort if error.
//
SDL_Surface* 
Load_Image(char *szFname)
{
	SDL_Surface *pSurface = NULL;

	pSurface = IMG_Load(szFname);

	if (pSurface == NULL)
	{
		fprintf(stderr, "Error loading image %s: %s\n", szFname, IMG_GetError());
		exit(1);
	}

	return pSurface;
}



//
//	Load_Sound() - Load a wav sound file, abort if fail.
//
Mix_Chunk* 
Load_Sound(char *szFname)
{
	Mix_Chunk *pChunk = NULL;

	pChunk = Mix_LoadWAV(szFname);

	if (pChunk == NULL)
	{
		fprintf(stderr, "Error loading sound %s: %s\n", szFname, Mix_GetError());
		exit(1);
	}

	return pChunk;
}

