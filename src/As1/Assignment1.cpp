//
// Subject:			SM2603 - 2D Game Programming
// Assignment:		1
// Student Name:	Cheung Kwok Wai
// Student Number:	52012205
// Date:			2/13/2012
//
// Assignment1.cpp - This is the skeletion file for Assignment 1
//
// requires static linkage to:
// sdl.lib, sdlmain.lib
//
// requires dynamic linkage to:
// sdl.dll
//

#include "..\basic_project\basic_project_main.h"


class Assignment1 : public Basic_SDL
{
private:

public:
	inline void AS1_DrawTriangle(int x0, int y0, int x1, int y1, int x2, int y2,
		Uint32 dwColor, SDL_Surface* pDisplaySurface)
	{
		GL_DrawLine(x0,y0,x1,y1,dwColor,pDisplaySurface);
		GL_DrawLine(x0,y0,x2,y2,dwColor,pDisplaySurface);
		GL_DrawLine(x1,y1,x2,y2,dwColor,pDisplaySurface);

	}
	Assignment1() : Basic_SDL(800,640){}

	void OnRender()
	{
		static Uint32 cBackground = GL_CreateColor(0,0,0);
		GL_DrawBackground(cBackground);										// draw background

		SDL_Surface *pDisplaySurface = this->GL_GetMainDisplay();			// video screen
		static Uint32 dwColor = GL_CreateColor(255,0,0);

		// Draw the character
		GL_DrawLine(130, 300, 50, 140, dwColor, pDisplaySurface);
		GL_DrawLine(270, 300, 430, 220, dwColor, pDisplaySurface);
		GL_DrawBox(130, 250, 270, 400, dwColor, pDisplaySurface);
		GL_DrawCircle(200, 200, 50, dwColor, pDisplaySurface);
		AS1_DrawTriangle(200, 400, 100, 500, 300, 500, dwColor, pDisplaySurface);

	}

};

int main(int argc, char *argv[])
{
	Assignment1 sdl;
	return sdl.OnExecute();
}
