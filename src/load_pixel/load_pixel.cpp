#include "..\basic_project\basic_project_main.h"


class test_bitmap : public Basic_SDL
{
private:
	SDL_Surface *message;		// the "Hello World" bitmap
	SDL_Surface *background;		// background image
	SDL_Surface *text;
public:
	test_bitmap() : Basic_SDL(800,600)
	{

	}
	bool OnInit()
	{
		if(!Basic_SDL::OnInit()) return false;

		message = GL_LoadImage( "hello_world.bmp" );		// the "Hello World" bitmap
		background = GL_LoadImage( "background.bmp" );		// background image

		text = GL_CreateText("Hello",GL_CreateColor(255,255,255));
			return true;
	}
	void OnRender()
	{
		SDL_Surface *screen = this->GL_GetMainDisplay();			// video screen

		static Uint32 cBackground = GL_CreateColor(55,55,55);
		GL_DrawBackground(cBackground);

		//Apply the background to the screens
		if(background)
			GL_ApplySurface( 0, 0, background, screen );

		//Apply the message to the screen
		if(message)
			GL_ApplySurface( 180, 140, message, screen );

        static int rollX = 0;
        rollX++;
        if(rollX > m_screen_width)
            rollX = 0;

		GL_DrawPixel(rollX,50,GL_CreateColor(50,100,50),screen);

		GL_ApplySurface(0,0,text,screen);

		
		//SDL_UpdateRect(screen, 0, 0, 0, 0);


	}
	void OnCleanup()
	{
		 //Free the surfaces

		SDL_FreeSurface( background );
		if(message)
			SDL_FreeSurface( message );
		Basic_SDL::OnCleanup();
	}
	void OnEvent(SDL_Event* evt)
	{
        switch(evt->type)
        {
            case SDL_KEYDOWN:
				SDL_FreeSurface(message);
				message = 0;
            break;
        }
	    Basic_SDL::OnEvent(evt);
	}

};

int main(int argc, char *argv[])
{


	test_bitmap sdl;


	return sdl.OnExecute();
}
