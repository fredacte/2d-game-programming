2D Game Programming			Author:	George Cheung Kwok Wai
Version Control Using Git: (link for public)
https://bitbucket.org/fredacte/2d-game-programming

Date:	2/3/2012

PP3
1.go to project\vs2008 and run "2D Programming.sln"
2.select five_element Project and run debug mode

Date:	3/9/2012

Assignment 2
1.go to project\vs2008 and run "2D Programming.sln"
2.select Assignment2 Project and run debug mode

Date:	2/14/2012

Assignment 1
1.go to project\vs2008 and run "2D Programming.sln"
2.select Assignment1 Project and run debug mode


Path Setup:

Runtime 		bin\
Project(vs2005) 	project\vs2005
Project(vs2008) 	project\vs2008
Project(vs2010) 	project\vs2010
SDL Header		sdl_dependency\include
SDL Library		sdl_dependency\lib
Boost Library		boost
Tinyxml Library		tinyxml


